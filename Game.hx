import h2d.Text;
import hxd.res.DefaultFont;
import h2d.Bitmap;
import src.Background;
import src.Enemy;
import src.Ship;
import hxd.Key in K;

enum GameStates {
	START_SCREEN;
	TO_GAME;
	IN_GAME;
	TO_GAME_OVER;
	GAME_OVER;
}
class Game extends hxd.App {
	var ship:Ship;
	var background:Background;
	var enemies:Array<Enemy> = [];
	var frames_after_enemy = 0;
	var score = 0;
	var font:h2d.Font;
	var start_text:h2d.Text;
	var hp_text:h2d.Text;
	var score_text:h2d.Text;
	var game_state:GameStates;
	var high_score = 0;
	var high_score_text:h2d.Text;
	var game_over_text:h2d.Text;

	override function init() {
		game_state = START_SCREEN;
		initBackground();
		initText();
		loadHighScore();
	}

	override function update(_) {
		switch (game_state) {
			case START_SCREEN:
				checkForStart();

			case TO_GAME:
				startGame();

			case IN_GAME:
				updateShipPosition();
				updateAssets();
				checkForCollisions();
				updateHealthAndScore();

			case TO_GAME_OVER:

			case GAME_OVER:
				checkForStart();
		}
		background.update();
	}

	function initText() {
		// LAUNCH SCREEN TEXT
		font = DefaultFont.get();
		start_text = new Text(font);
		start_text.text = "Tap to begin!";
		start_text.textAlign = Center;
		start_text.x = Std.int(s2d.width/2);
		start_text.y = Std.int(s2d.height/2);
		start_text.scale(3);
		start_text.alpha = 1.0;
		s2d.addChild(start_text);

		high_score_text = new Text(font);
		high_score_text.text = 'HIGH SCORE: $high_score';
		high_score_text.textAlign = Center;
		high_score_text.x = Std.int(s2d.width/2);
		high_score_text.y = Std.int(3*s2d.height/4);
		high_score_text.scale(2);
		s2d.addChild(high_score_text);
		
		// IN GAME TEXT
		hp_text = new Text(font);
		hp_text.text = "HP:100";
		hp_text.textAlign = Left;
		hp_text.x = 20;
		hp_text.y = 17;
		hp_text.alpha = 0.0;
		hp_text.setScale(2);
		s2d.addChild(hp_text);

		score_text = new Text(font);
		score_text.text = "SCORE:0";
		score_text.textAlign = Right;
		score_text.x = s2d.width-20;
		score_text.y = 17;
		score_text.alpha = 0.0;
		score_text.setScale(2);
		s2d.addChild(score_text);

		// GAME OVER TEXT
		game_over_text = new Text(font);
		game_over_text.text = "GAME OVER";
		game_over_text.textAlign = Center;
		game_over_text.textColor = 0xff0000;
		game_over_text.x = Std.int(s2d.width/2);
		game_over_text.y = Std.int(s2d.height/4);
		game_over_text.setScale(4);
		game_over_text.alpha = 0.0;
		s2d.addChild(game_over_text);


	}

	function loadHighScore() {
		// load previous high score stored somewhere
	}

	function checkForStart() {
		// CONDITION FOR HASHLINK
		if (K.isPressed("X".code) || K.isDown("X".code)) game_state = TO_GAME;

	}

	function updateHighScore() {
		if (score > high_score) high_score = score;
	}

	function initBackground() {
		background = new Background(s2d);
	}

	function updateHealthAndScore() {
		hp_text.text = 'HP:${ship.getHealth()}';
		score_text.text = 'SCORE:$score';
	}

	function startGame() {
		ship = new Ship(s2d);
		ship.setScale(2);
		ship.setPosition(Std.int(s2d.width/2 - 14), Std.int(3 * s2d.height / 4));
		game_over_text.alpha = 0;
		start_text.alpha = 0.0;
		high_score_text.alpha = 0.0;
		hp_text.alpha = 1.0;
		score_text.alpha = 1.0;
		game_state = IN_GAME;
	}

	function gameOver() {
		updateHighScore();
		for (enemy in enemies) enemy.die();
		enemies = [];
		score = 0;
		high_score_text.text = 'HIGH SCORE: $high_score';
		hp_text.alpha = 0;
		score_text.alpha = 0;
		game_over_text.alpha = 1;
		start_text.alpha = 1;
		high_score_text.alpha = 1;
		game_state = GAME_OVER;
	}

	function updateAssets() {
		ship.update();
		for (enemy in enemies) enemy.update();
	}

	function updateShipPosition() {
		if (K.isPressed("W".code) || K.isDown("W".code)) ship.move(0);
		if (K.isPressed("D".code) || K.isDown("D".code)) ship.move(1);
		if (K.isPressed("S".code) || K.isDown("S".code)) ship.move(2);
		if (K.isPressed("A".code) || K.isDown("A".code)) ship.move(3);
	}

	function checkForCollisions() {
		var hitting_projectile:Bitmap = null;

		for (enemy in enemies) {
			hitting_projectile = ship.isHit(enemy.getProjectiles());
			if (hitting_projectile != null) {
				ship.takeDamage(enemy.getDamage());
				enemy.removeProjectile(hitting_projectile);
				hitting_projectile.remove();
				hitting_projectile = null;
				if (ship.isDed()) {
					ship.die();
					gameOver();
				}
			}
			hitting_projectile = enemy.isHit(ship.getProjectiles());
			if (hitting_projectile != null) {
				enemy.takeDamage(ship.getDamage());
				ship.removeProjectile(hitting_projectile);
				hitting_projectile.remove();
				hitting_projectile = null;

				if (enemy.isDed()) {
					score += enemy.score;
					enemy.die();
					enemies.remove(enemy);
				}
			}
		}
		if (frames_after_enemy == 80) {
			enemies.push(new Enemy(s2d, 2));
			frames_after_enemy = 0;
		} else if (enemies.length < 8) {
			frames_after_enemy++;
		}
	}

	function displayHpAndScore() {

	}

	static function main() {
		hxd.Res.initLocal();
		new Game();
	}
}
