package src;

import src.Star;

class Background {
    var far_stars:Array<Star>;
    var mid_stars:Array<Star>;
    var close_stars:Array<Star>;
    var scene:h2d.Scene;
    var far_stars_size = 20;
    var mid_stars_size = 3;
    var close_stars_size = 1;

    public function new(scene:h2d.Scene) {
        this.scene = scene;
        this.far_stars = [for (i in 0...far_stars_size) new Star(scene, 1, true)];
        this.mid_stars = [for (i in 0...mid_stars_size) new Star(scene, 2, true)];
        this.close_stars = [for (i in 0...close_stars_size) new Star(scene, 4, true)];
    }

    public function update() {
        far_stars = update_level(far_stars, 1);
        mid_stars = update_level(mid_stars, 2);
        close_stars = update_level(close_stars, 4);
    }

    function update_level(stars:Array<Star>, level:Int) {
        var stars_length = 0;

        switch (level) {
            case 1 : stars_length = far_stars_size;
            case 2 : stars_length = mid_stars_size;
            case 4 : stars_length = close_stars_size;
        }

        for (star in stars) star.update();
        stars = [for (star in stars) if (star.onScreen()) star];
        
        for (i in 0...stars_length-stars.length) {
            stars.push(new Star(scene, level, false));
        }

        return stars;
    }
}