package src;

import h2d.Bitmap;

class Star {
    var star:h2d.Bitmap;
    var scene:h2d.Scene;
    var scale:Int;

    public function new(scene:h2d.Scene, scale:Int, launch:Bool) {
        this.star = new Bitmap(hxd.Res.sprites.environment.star.toTile(), scene);

        if (launch) {
            this.star.x = Std.int(Math.random()*scene.width);
            this.star.y = Std.int(Math.random()*scene.height);
        } else {
            this.star.x = Std.int(Math.random()*scene.width);
            this.star.y = 0;
        }
        
        this.scene = scene;
        this.scale = scale;
        this.star.alpha = 0.8;
        this.star.setScale(scale);
    }

    public function update() {
        star.y = star.y + scale*scale*2;
    }

    public function onScreen() {
        return this.star.y < this.scene.height;
    }
}