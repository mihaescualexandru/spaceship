package src;

import h2d.Anim;
import h2d.Bitmap;

class Ship {
	var body:h2d.Bitmap;
	var booster:h2d.Anim;
	var left_booster:h2d.Anim;
	var right_booster:h2d.Anim;
	var level:Int;
	var health = 100;
    var damage = 35;
	var x:Int;
	var y:Int;
	var scale:Int;
    var speed = 2;
    var scene:h2d.Scene;
    var projectile_loading_tiles:Array<h2d.Tile>;
    var projectile_loading:h2d.Anim;
    var frames_between_shots = 40;
    var frame_count = 0;
    var active_projectiles:Array<h2d.Bitmap> = [];

	public function new(scene:h2d.Scene) {
        this.scene = scene;
		body = new Bitmap(hxd.Res.sprites.ship.one.toTile(), scene);

		var booster_tiles = hxd.Res.sprites.booster.one_main.toTile().split(3, false);
		var side_booster_tiles = hxd.Res.sprites.booster.one_side.toTile().split(3, false);

		booster = new Anim(booster_tiles, scene);
		left_booster = new Anim(side_booster_tiles, scene);
		right_booster = new Anim(side_booster_tiles, scene);
        booster.loop = true;
		booster.fading = true;

		left_booster.loop = true;
		left_booster.fading = true;

		right_booster.loop = true;
		right_booster.fading = true;

        projectile_loading_tiles = hxd.Res.sprites.projectile.one.loading.toTile().split(3, false);
        projectile_loading = new Anim(projectile_loading_tiles, scene);
        projectile_loading.fading = true;

		this.scale = 1;
	}

    public function update() {
        for (proj in active_projectiles) {
            proj.y = proj.y - 5;
        }

        active_projectiles = [for (proj in active_projectiles) if (proj.y > -100) proj];

        if (frame_count == frames_between_shots) {
            projectile_loading.play(projectile_loading_tiles);
            projectile_loading.loop = false;
            frame_count = 0;
        }
        if (frame_count == 10) {
            active_projectiles.unshift(new Bitmap(hxd.Res.sprites.projectile.one.projectile.toTile(), scene));
            active_projectiles[0].setScale(scale);
            active_projectiles[0].x= this.x+11*scale;
            active_projectiles[0].y = this.y-4*scale;
        }
        frame_count += 1;
    }

	public function setPosition(x:Int, y:Int) {
        if (x > 0 && x < scene.width-28*scale) this.x = Std.int(x);
        if (y > 0 && y < scene.height-28*scale) this.y = Std.int(y);

		body.x = this.x;
		body.y = this.y;
		booster.x = this.x + Std.int(7 * scale);
		booster.y = this.y + Std.int(27 * scale);

		left_booster.x = this.x + Std.int(1 * scale);
		left_booster.y = this.y + Std.int(26 * scale);

		right_booster.x = this.x + Std.int(19 * scale);
		right_booster.y = this.y + Std.int(26 * scale);

        projectile_loading.x = this.x+11*scale;
        projectile_loading.y = this.y-4*scale;
	}

    public function move(direction:Int) {
        switch direction {
            case 0 : setPosition(this.x, this.y-speed*scale);
            case 1 : setPosition(this.x+speed*scale, this.y);
            case 2 : setPosition(this.x, this.y+speed*scale);
            case 3 : setPosition(this.x-speed*scale, this.y);
        }
    }

	public function setScale(scale:Int) {
		this.scale = scale;
		body.setScale(scale);
		booster.setScale(scale);
		left_booster.setScale(scale);
		right_booster.setScale(scale);
        projectile_loading.setScale(scale);
	}

    function isOverlap(x, y) {
        // trace(this.x, this.y, x, y);
        if (this.x > x + 7*scale || x > this.x + 28*scale) return false;
        if (this.y > y + 7*scale || y > this.y + 28*scale) return false;
        return true; 
    }

    public function isHit(projectiles:Array<h2d.Bitmap>) {
        for (projectile in projectiles) {
            if (isOverlap(Std.int(projectile.x), Std.int(projectile.y))) return projectile;
        }
        return null;
    }

    public function getDamage() {
        return damage;
    }

    public function getHealth() {
        return health;
    }

    public function getProjectiles() {
        return active_projectiles;
    }

    public function takeDamage(damageTaken:Int) {
        health -= damageTaken;
        if (health <= 0) die();
    }

    public function removeProjectile(projectile:Bitmap) {
        active_projectiles.remove(projectile);
    }

    public function isDed() {
        return health <= 0;
    }

    public function die() {
        body.remove();
        booster.remove();
        left_booster.remove();
        right_booster.remove();
        for (projectile in active_projectiles) projectile.remove();
    }
}
