package src;

import h3d.anim.SmoothTransition.SmoothedObject;
import h2d.Bitmap;
import h2d.Anim;

class Enemy {
    var scene:h2d.Scene;
    var enemy:h2d.Anim;
    var projectiles:Array<h2d.Bitmap> = [];
    var scale:Int;
    var frames_between_shots = 40;
    var frame_count = 0;
    var on_path = false;
    var path = 0;
    var health = 100;
    var damage = 20;
    public final score = 20;

    public function new(scene:h2d.Scene, scale:Int) {
        this.scene = scene;
        this.scale = scale;
        enemy = new h2d.Anim(hxd.Res.sprites.enemy.one.enemy.toTile().split(3, false), scene);
        enemy.loop = true;
        enemy.fading = true;
        enemy.setScale(scale);
        enemy.x = 80;
        enemy.y = -100;
    }

    public function update() {
        if (!on_path) {
            enemy.y += 1;
            if (enemy.y == 80) {
                on_path = true;
            }
        } else {
            switch (path) {
                case 0: 
                    enemy.y++;
                    if (enemy.y == 100) path = 1;
                case 1:
                    enemy.x++;
                    if (enemy.x == scene.width - 80 - 20*scale) path = 2;
                case 2:
                    enemy.y--;
                    if (enemy.y == 40) path = 3;
                case 3:
                    enemy.x--;
                    if (enemy.x == 80) path = 0;
            }
        }

        if (on_path && Math.random() < 0.005) fire();

        for (projectile in projectiles) {
            projectile.y += 2*scale;
        }
        
        projectiles = [for (projectile in projectiles) if (projectile.y < scene.height + 50) projectile]; 
        frame_count += 1;
    }

    function fire() {
        projectiles.unshift(new Bitmap(hxd.Res.sprites.enemy.one.projectile.toTile(), scene));
        projectiles[0].setScale(scale);
        projectiles[0].x = enemy.x + 7*scale;
        projectiles[0].y = enemy.y + 18*scale;
    }

    public function getBounds() {
        return [[enemy.x, enemy.y], [enemy.x+20*scale, enemy.y+20*scale]];
    }

    public function getProjectiles() {
        return projectiles;
    }

    public function getDamage() {
        return damage;
    }

    function isOverlap(x, y) {
        // trace(enemy.x, enemy.y, x, y);
        if (enemy.x > x + 6*scale || x > enemy.x + 20*scale) return false;
        if (enemy.y > y + 6*scale || y > enemy.y + 20*scale) return false;
        return true; 
    }

    public function isHit(projectiles:Array<h2d.Bitmap>) {
        for (projectile in projectiles) {
            if (isOverlap(Std.int(projectile.x), Std.int(projectile.y))) return projectile;
        }
        return null;
    }

    public function takeDamage(damageTaken:Int) {
        health -= damageTaken;
    }

    public function removeProjectile(projectile:Bitmap) {
        projectiles.remove(projectile);
    }

    public function isDed() {
        return health <= 0;
    }

    public function die() {
        enemy.remove();
        for (projectile in projectiles) projectile.remove();
    }
}