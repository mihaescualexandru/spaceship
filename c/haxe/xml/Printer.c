﻿// Generated by HLC 4.0.5 (HL v4)
#define HLC_BOOT
#include <hlc.h>
#include <haxe/xml/Printer.h>
extern hl_type t$haxe_xml_Printer;
void haxe_xml_Printer_new(haxe__xml__Printer,bool);
extern String s$;
void haxe_xml_Printer_writeNode(haxe__xml__Printer,Xml,String);
String StringBuf_toString(StringBuf);
extern hl_type t$StringBuf;
void StringBuf_new(StringBuf);
#include <_std/Xml.h>
#include <hl/types/ArrayObj.h>
#include <_std/EReg.h>
extern String s$524a507;
String String___add__(String,String);
void StringBuf_add(StringBuf,vdynamic*);
extern $Xml g$_Xml;
extern String s$63ec124;
String _Xml_XmlType_Impl__toString(int);
vvirtual* Xml_attributes(Xml);
extern hl_type t$fun_bf7849e;
extern hl_type t$fun_820f49a;
extern String s$7215ee9;
extern String s$15b400f;
String Xml_get(Xml,String);
String StringTools_htmlEscape(String,vdynamic*);
extern String s$b15835f;
bool haxe_xml_Printer_hasChildren(haxe__xml__Printer,Xml);
extern String s$cedf8da;
extern String s$68b329d;
extern String s$925902a;
vvirtual* hl_types_ArrayObj_iterator(hl__types__ArrayObj);
extern hl_type t$vrt_15cf21a;
extern hl_type t$fun_efa96ab;
extern String s$5e732a1;
extern String s$0c69b6d;
extern String s$18f68d1;
extern String s$Bad_node_type_unexpected_;
extern String s$_CDATA_;
extern String s$a493bdd;
extern hl_type t$EReg;
extern String s$7d574b5;
extern String s$g;
void EReg_new(EReg,String,String);
String EReg_replace(EReg,String,String);
extern String s$c11bb7e;
extern String s$9a73e5b;
String StringTools_trim(String);
extern String s$_DOCTYPE_;
extern String s$b5a6ed8;
extern String s$6ec256c;
String StringTools_ltrim(String);

String haxe_xml_Printer_print(Xml r0,vdynamic* r1) {
	String r5;
	bool r2;
	haxe__xml__Printer r3;
	StringBuf r6;
	if( r1 ) goto label$417020e_1_3;
	r2 = false;
	r1 = hl_alloc_dynbool(r2);
	label$417020e_1_3:
	r3 = (haxe__xml__Printer)hl_alloc_obj(&t$haxe_xml_Printer);
	r2 = r1 ? r1->v.b : 0;
	haxe_xml_Printer_new(r3,r2);
	r5 = (String)s$;
	haxe_xml_Printer_writeNode(r3,r0,r5);
	r6 = r3->output;
	if( r6 == NULL ) hl_null_access();
	r5 = StringBuf_toString(r6);
	return r5;
}

void haxe_xml_Printer_new(haxe__xml__Printer r0,bool r1) {
	StringBuf r2;
	r2 = (StringBuf)hl_alloc_obj(&t$StringBuf);
	StringBuf_new(r2);
	r0->output = r2;
	r0->pretty = r1;
	return;
}

void haxe_xml_Printer_writeNode(haxe__xml__Printer r0,Xml r1,String r2) {
	String r7, r8, r13;
	hl__types__ArrayObj r16;
	vvirtual *r11, *r15, *r17;
	bool r12;
	EReg r18;
	$Xml r10;
	Xml r5;
	StringBuf r6;
	vdynamic *r14;
	int r4, r9;
	if( r1 == NULL ) hl_null_access();
	r4 = r1->nodeType;
	switch(r4) {
		default:
			goto label$417020e_3_316;
		case 0:
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			r8 = (String)s$524a507;
			r7 = String___add__(r2,r8);
			StringBuf_add(r6,((vdynamic*)r7));
			r4 = r1->nodeType;
			r10 = ($Xml)g$_Xml;
			r9 = r10->Element;
			if( r4 == r9 ) goto label$417020e_3_18;
			r7 = (String)s$63ec124;
			r4 = r1->nodeType;
			r8 = _Xml_XmlType_Impl__toString(r4);
			r7 = String___add__(r7,r8);
			hl_throw((vdynamic*)r7);
			label$417020e_3_18:
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			r7 = r1->nodeName;
			StringBuf_add(r6,((vdynamic*)r7));
			r11 = Xml_attributes(r1);
			label$417020e_3_23:
			if( r11 == NULL ) hl_null_access();
			if( hl_vfields(r11)[0] ) r12 = ((bool (*)(vdynamic*))hl_vfields(r11)[0])(r11->value); else {
				vdynamic ret;
				hl_dyn_call_obj(r11->value,&t$fun_bf7849e,407283053/*hasNext*/,NULL,&ret);
				r12 = (bool)ret.v.i;
			}
			if( !r12 ) goto label$417020e_3_48;
			if( hl_vfields(r11)[1] ) r7 = ((String (*)(vdynamic*))hl_vfields(r11)[1])(r11->value); else {
				r7 = (String)hl_dyn_call_obj(r11->value,&t$fun_820f49a,151160317/*next*/,NULL,NULL);
			}
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			r8 = (String)s$7215ee9;
			r8 = String___add__(r8,r7);
			r13 = (String)s$15b400f;
			r8 = String___add__(r8,r13);
			StringBuf_add(r6,((vdynamic*)r8));
			if( r1 == NULL ) hl_null_access();
			r8 = Xml_get(r1,r7);
			r12 = true;
			r14 = hl_alloc_dynbool(r12);
			r8 = StringTools_htmlEscape(r8,r14);
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			StringBuf_add(r6,((vdynamic*)r8));
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			r8 = (String)s$b15835f;
			StringBuf_add(r6,((vdynamic*)r8));
			goto label$417020e_3_23;
			label$417020e_3_48:
			r12 = haxe_xml_Printer_hasChildren(r0,r1);
			if( !r12 ) goto label$417020e_3_121;
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			r7 = (String)s$cedf8da;
			StringBuf_add(r6,((vdynamic*)r7));
			r12 = r0->pretty;
			if( !r12 ) goto label$417020e_3_60;
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			r7 = (String)s$68b329d;
			StringBuf_add(r6,((vdynamic*)r7));
			label$417020e_3_60:
			if( r1 == NULL ) hl_null_access();
			r4 = r1->nodeType;
			r10 = ($Xml)g$_Xml;
			r9 = r10->Document;
			if( r4 == r9 ) goto label$417020e_3_74;
			r4 = r1->nodeType;
			r10 = ($Xml)g$_Xml;
			r9 = r10->Element;
			if( r4 == r9 ) goto label$417020e_3_74;
			r7 = (String)s$925902a;
			r4 = r1->nodeType;
			r8 = _Xml_XmlType_Impl__toString(r4);
			r7 = String___add__(r7,r8);
			hl_throw((vdynamic*)r7);
			label$417020e_3_74:
			r16 = r1->children;
			if( r16 == NULL ) hl_null_access();
			r15 = hl_types_ArrayObj_iterator(r16);
			r17 = hl_to_virtual(&t$vrt_15cf21a,(vdynamic*)r15);
			label$417020e_3_78:
			if( r17 == NULL ) hl_null_access();
			if( hl_vfields(r17)[0] ) r12 = ((bool (*)(vdynamic*))hl_vfields(r17)[0])(r17->value); else {
				vdynamic ret;
				hl_dyn_call_obj(r17->value,&t$fun_bf7849e,407283053/*hasNext*/,NULL,&ret);
				r12 = (bool)ret.v.i;
			}
			if( !r12 ) goto label$417020e_3_91;
			if( hl_vfields(r17)[1] ) r5 = ((Xml (*)(vdynamic*))hl_vfields(r17)[1])(r17->value); else {
				r5 = (Xml)hl_dyn_call_obj(r17->value,&t$fun_efa96ab,151160317/*next*/,NULL,NULL);
			}
			r12 = r0->pretty;
			if( !r12 ) goto label$417020e_3_88;
			r8 = (String)s$5e732a1;
			r7 = String___add__(r2,r8);
			goto label$417020e_3_89;
			label$417020e_3_88:
			r7 = r2;
			label$417020e_3_89:
			haxe_xml_Printer_writeNode(r0,r5,r7);
			goto label$417020e_3_78;
			label$417020e_3_91:
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			r8 = (String)s$0c69b6d;
			r7 = String___add__(r2,r8);
			StringBuf_add(r6,((vdynamic*)r7));
			if( r1 == NULL ) hl_null_access();
			r4 = r1->nodeType;
			r10 = ($Xml)g$_Xml;
			r9 = r10->Element;
			if( r4 == r9 ) goto label$417020e_3_106;
			r7 = (String)s$63ec124;
			r4 = r1->nodeType;
			r8 = _Xml_XmlType_Impl__toString(r4);
			r7 = String___add__(r7,r8);
			hl_throw((vdynamic*)r7);
			label$417020e_3_106:
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			r7 = r1->nodeName;
			StringBuf_add(r6,((vdynamic*)r7));
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			r7 = (String)s$cedf8da;
			StringBuf_add(r6,((vdynamic*)r7));
			r12 = r0->pretty;
			if( !r12 ) goto label$417020e_3_120;
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			r7 = (String)s$68b329d;
			StringBuf_add(r6,((vdynamic*)r7));
			label$417020e_3_120:
			goto label$417020e_3_131;
			label$417020e_3_121:
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			r7 = (String)s$18f68d1;
			StringBuf_add(r6,((vdynamic*)r7));
			r12 = r0->pretty;
			if( !r12 ) goto label$417020e_3_131;
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			r7 = (String)s$68b329d;
			StringBuf_add(r6,((vdynamic*)r7));
			label$417020e_3_131:
			goto label$417020e_3_316;
		case 1:
			r4 = r1->nodeType;
			r10 = ($Xml)g$_Xml;
			r9 = r10->Document;
			if( r4 == r9 ) goto label$417020e_3_140;
			r4 = r1->nodeType;
			r10 = ($Xml)g$_Xml;
			r9 = r10->Element;
			if( r4 != r9 ) goto label$417020e_3_145;
			label$417020e_3_140:
			r7 = (String)s$Bad_node_type_unexpected_;
			r4 = r1->nodeType;
			r8 = _Xml_XmlType_Impl__toString(r4);
			r7 = String___add__(r7,r8);
			hl_throw((vdynamic*)r7);
			label$417020e_3_145:
			r7 = r1->nodeValue;
			if( r7 == NULL ) hl_null_access();
			r4 = r7->length;
			r9 = 0;
			if( r4 == r9 ) goto label$417020e_3_162;
			r14 = NULL;
			r13 = StringTools_htmlEscape(r7,r14);
			r8 = String___add__(r2,r13);
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			StringBuf_add(r6,((vdynamic*)r8));
			r12 = r0->pretty;
			if( !r12 ) goto label$417020e_3_162;
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			r8 = (String)s$68b329d;
			StringBuf_add(r6,((vdynamic*)r8));
			label$417020e_3_162:
			goto label$417020e_3_316;
		case 2:
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			r8 = (String)s$_CDATA_;
			r7 = String___add__(r2,r8);
			StringBuf_add(r6,((vdynamic*)r7));
			r4 = r1->nodeType;
			r10 = ($Xml)g$_Xml;
			r9 = r10->Document;
			if( r4 == r9 ) goto label$417020e_3_176;
			r4 = r1->nodeType;
			r10 = ($Xml)g$_Xml;
			r9 = r10->Element;
			if( r4 != r9 ) goto label$417020e_3_181;
			label$417020e_3_176:
			r7 = (String)s$Bad_node_type_unexpected_;
			r4 = r1->nodeType;
			r8 = _Xml_XmlType_Impl__toString(r4);
			r7 = String___add__(r7,r8);
			hl_throw((vdynamic*)r7);
			label$417020e_3_181:
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			r7 = r1->nodeValue;
			StringBuf_add(r6,((vdynamic*)r7));
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			r7 = (String)s$a493bdd;
			StringBuf_add(r6,((vdynamic*)r7));
			r12 = r0->pretty;
			if( !r12 ) goto label$417020e_3_195;
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			r7 = (String)s$68b329d;
			StringBuf_add(r6,((vdynamic*)r7));
			label$417020e_3_195:
			goto label$417020e_3_316;
		case 3:
			r4 = r1->nodeType;
			r10 = ($Xml)g$_Xml;
			r9 = r10->Document;
			if( r4 == r9 ) goto label$417020e_3_204;
			r4 = r1->nodeType;
			r10 = ($Xml)g$_Xml;
			r9 = r10->Element;
			if( r4 != r9 ) goto label$417020e_3_209;
			label$417020e_3_204:
			r7 = (String)s$Bad_node_type_unexpected_;
			r4 = r1->nodeType;
			r8 = _Xml_XmlType_Impl__toString(r4);
			r7 = String___add__(r7,r8);
			hl_throw((vdynamic*)r7);
			label$417020e_3_209:
			r7 = r1->nodeValue;
			r18 = (EReg)hl_alloc_obj(&t$EReg);
			r8 = (String)s$7d574b5;
			r13 = (String)s$g;
			EReg_new(r18,r8,r13);
			r13 = (String)s$;
			r8 = EReg_replace(r18,r7,r13);
			r7 = r8;
			r8 = (String)s$c11bb7e;
			r8 = String___add__(r8,r7);
			r13 = (String)s$9a73e5b;
			r8 = String___add__(r8,r13);
			r7 = r8;
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			StringBuf_add(r6,((vdynamic*)r2));
			r8 = StringTools_trim(r7);
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			StringBuf_add(r6,((vdynamic*)r8));
			r12 = r0->pretty;
			if( !r12 ) goto label$417020e_3_235;
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			r8 = (String)s$68b329d;
			StringBuf_add(r6,((vdynamic*)r8));
			label$417020e_3_235:
			goto label$417020e_3_316;
		case 4:
			r4 = r1->nodeType;
			r10 = ($Xml)g$_Xml;
			r9 = r10->Document;
			if( r4 == r9 ) goto label$417020e_3_244;
			r4 = r1->nodeType;
			r10 = ($Xml)g$_Xml;
			r9 = r10->Element;
			if( r4 != r9 ) goto label$417020e_3_249;
			label$417020e_3_244:
			r7 = (String)s$Bad_node_type_unexpected_;
			r4 = r1->nodeType;
			r8 = _Xml_XmlType_Impl__toString(r4);
			r7 = String___add__(r7,r8);
			hl_throw((vdynamic*)r7);
			label$417020e_3_249:
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			r7 = (String)s$_DOCTYPE_;
			r8 = r1->nodeValue;
			r7 = String___add__(r7,r8);
			r8 = (String)s$cedf8da;
			r7 = String___add__(r7,r8);
			StringBuf_add(r6,((vdynamic*)r7));
			r12 = r0->pretty;
			if( !r12 ) goto label$417020e_3_263;
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			r7 = (String)s$68b329d;
			StringBuf_add(r6,((vdynamic*)r7));
			label$417020e_3_263:
			goto label$417020e_3_316;
		case 5:
			r4 = r1->nodeType;
			r10 = ($Xml)g$_Xml;
			r9 = r10->Document;
			if( r4 == r9 ) goto label$417020e_3_272;
			r4 = r1->nodeType;
			r10 = ($Xml)g$_Xml;
			r9 = r10->Element;
			if( r4 != r9 ) goto label$417020e_3_277;
			label$417020e_3_272:
			r7 = (String)s$Bad_node_type_unexpected_;
			r4 = r1->nodeType;
			r8 = _Xml_XmlType_Impl__toString(r4);
			r7 = String___add__(r7,r8);
			hl_throw((vdynamic*)r7);
			label$417020e_3_277:
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			r7 = (String)s$b5a6ed8;
			r8 = r1->nodeValue;
			r7 = String___add__(r7,r8);
			r8 = (String)s$6ec256c;
			r7 = String___add__(r7,r8);
			StringBuf_add(r6,((vdynamic*)r7));
			r12 = r0->pretty;
			if( !r12 ) goto label$417020e_3_291;
			r6 = r0->output;
			if( r6 == NULL ) hl_null_access();
			r7 = (String)s$68b329d;
			StringBuf_add(r6,((vdynamic*)r7));
			label$417020e_3_291:
			goto label$417020e_3_316;
		case 6:
			r4 = r1->nodeType;
			r10 = ($Xml)g$_Xml;
			r9 = r10->Document;
			if( r4 == r9 ) goto label$417020e_3_305;
			r4 = r1->nodeType;
			r10 = ($Xml)g$_Xml;
			r9 = r10->Element;
			if( r4 == r9 ) goto label$417020e_3_305;
			r7 = (String)s$925902a;
			r4 = r1->nodeType;
			r8 = _Xml_XmlType_Impl__toString(r4);
			r7 = String___add__(r7,r8);
			hl_throw((vdynamic*)r7);
			label$417020e_3_305:
			r16 = r1->children;
			if( r16 == NULL ) hl_null_access();
			r15 = hl_types_ArrayObj_iterator(r16);
			r17 = hl_to_virtual(&t$vrt_15cf21a,(vdynamic*)r15);
			label$417020e_3_309:
			if( r17 == NULL ) hl_null_access();
			if( hl_vfields(r17)[0] ) r12 = ((bool (*)(vdynamic*))hl_vfields(r17)[0])(r17->value); else {
				vdynamic ret;
				hl_dyn_call_obj(r17->value,&t$fun_bf7849e,407283053/*hasNext*/,NULL,&ret);
				r12 = (bool)ret.v.i;
			}
			if( !r12 ) goto label$417020e_3_316;
			if( hl_vfields(r17)[1] ) r5 = ((Xml (*)(vdynamic*))hl_vfields(r17)[1])(r17->value); else {
				r5 = (Xml)hl_dyn_call_obj(r17->value,&t$fun_efa96ab,151160317/*next*/,NULL,NULL);
			}
			haxe_xml_Printer_writeNode(r0,r5,r2);
			goto label$417020e_3_309;
	}
	label$417020e_3_316:
	return;
}

bool haxe_xml_Printer_hasChildren(haxe__xml__Printer r0,Xml r1) {
	String r6, r7;
	hl__types__ArrayObj r9;
	vvirtual *r8, *r10;
	bool r11;
	$Xml r5;
	Xml r3;
	int r2, r4;
	if( r1 == NULL ) hl_null_access();
	r2 = r1->nodeType;
	r5 = ($Xml)g$_Xml;
	r4 = r5->Document;
	if( r2 == r4 ) goto label$417020e_4_14;
	r2 = r1->nodeType;
	r5 = ($Xml)g$_Xml;
	r4 = r5->Element;
	if( r2 == r4 ) goto label$417020e_4_14;
	r6 = (String)s$925902a;
	r2 = r1->nodeType;
	r7 = _Xml_XmlType_Impl__toString(r2);
	r6 = String___add__(r6,r7);
	hl_throw((vdynamic*)r6);
	label$417020e_4_14:
	r9 = r1->children;
	if( r9 == NULL ) hl_null_access();
	r8 = hl_types_ArrayObj_iterator(r9);
	r10 = hl_to_virtual(&t$vrt_15cf21a,(vdynamic*)r8);
	label$417020e_4_18:
	if( r10 == NULL ) hl_null_access();
	if( hl_vfields(r10)[0] ) r11 = ((bool (*)(vdynamic*))hl_vfields(r10)[0])(r10->value); else {
		vdynamic ret;
		hl_dyn_call_obj(r10->value,&t$fun_bf7849e,407283053/*hasNext*/,NULL,&ret);
		r11 = (bool)ret.v.i;
	}
	if( !r11 ) goto label$417020e_4_51;
	if( hl_vfields(r10)[1] ) r3 = ((Xml (*)(vdynamic*))hl_vfields(r10)[1])(r10->value); else {
		r3 = (Xml)hl_dyn_call_obj(r10->value,&t$fun_efa96ab,151160317/*next*/,NULL,NULL);
	}
	if( r3 == NULL ) hl_null_access();
	r2 = r3->nodeType;
	switch(r2) {
		default:
			goto label$417020e_4_50;
		case 0:
		case 1:
			r11 = true;
			return r11;
		case 2:
		case 3:
			r2 = r3->nodeType;
			r5 = ($Xml)g$_Xml;
			r4 = r5->Document;
			if( r2 == r4 ) goto label$417020e_4_37;
			r2 = r3->nodeType;
			r5 = ($Xml)g$_Xml;
			r4 = r5->Element;
			if( r2 != r4 ) goto label$417020e_4_42;
			label$417020e_4_37:
			r6 = (String)s$Bad_node_type_unexpected_;
			r2 = r3->nodeType;
			r7 = _Xml_XmlType_Impl__toString(r2);
			r6 = String___add__(r6,r7);
			hl_throw((vdynamic*)r6);
			label$417020e_4_42:
			r6 = r3->nodeValue;
			r6 = StringTools_ltrim(r6);
			if( r6 == NULL ) hl_null_access();
			r2 = r6->length;
			r4 = 0;
			if( r2 == r4 ) goto label$417020e_4_50;
			r11 = true;
			return r11;
	}
	label$417020e_4_50:
	goto label$417020e_4_18;
	label$417020e_4_51:
	r11 = false;
	return r11;
}

