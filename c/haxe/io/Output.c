﻿// Generated by HLC 4.0.5 (HL v4)
#define HLC_BOOT
#include <hlc.h>
#include <haxe/io/Output.h>
extern String s$Not_implemented;
#include <haxe/io/Bytes.h>
#include <haxe/io/Error.h>
#include <haxe/io/BytesDataImpl.h>
extern venum* g$haxe_io_Error_OutsideBounds;
extern hl_type t$haxe_io_BytesDataImpl;
void haxe_io_BytesDataImpl_new(haxe__io__BytesDataImpl,vbyte*,int);
extern venum* g$haxe_io_Error_Blocked;
int haxe_io_FPHelper_floatToI32(float);
void haxe_io_Output_writeInt32(haxe__io__Output,int);
extern venum* g$haxe_io_Error_Overflow;
void haxe_io_Output_writeUInt16(haxe__io__Output,int);
#include <haxe/io/Encoding.h>
haxe__io__Bytes haxe_io_Bytes_ofString(String,venum*);

void haxe_io_Output_writeByte(haxe__io__Output r0,int r1) {
	String r2;
	r2 = (String)s$Not_implemented;
	hl_throw((vdynamic*)r2);
}

int haxe_io_Output_writeBytes(haxe__io__Output r0,haxe__io__Bytes r1,int r2,int r3) {
	venum *r7;
	haxe__io__BytesDataImpl r8, r10;
	vbyte *r9;
	int r5, r6, r11;
	r6 = 0;
	if( r2 < r6 ) goto label$380ee06_2_8;
	r6 = 0;
	if( r3 < r6 ) goto label$380ee06_2_8;
	r5 = r2 + r3;
	if( r1 == NULL ) hl_null_access();
	r6 = r1->length;
	if( r6 >= r5 ) goto label$380ee06_2_10;
	label$380ee06_2_8:
	r7 = (venum*)g$haxe_io_Error_OutsideBounds;
	hl_throw((vdynamic*)r7);
	label$380ee06_2_10:
	r8 = (haxe__io__BytesDataImpl)hl_alloc_obj(&t$haxe_io_BytesDataImpl);
	r9 = r1->b;
	r5 = r1->length;
	haxe_io_BytesDataImpl_new(r8,r9,r5);
	r10 = r8;
	r5 = r3;
	label$380ee06_2_16:
	r11 = 0;
	if( r11 >= r5 ) goto label$380ee06_2_26;
	if( r10 == NULL ) hl_null_access();
	r9 = r10->bytes;
	r6 = *(unsigned char*)(r9 + r2);
	((void (*)(haxe__io__Output,int))r0->$type->vobj_proto[0])(r0,r6);
	++r2;
	--r5;
	goto label$380ee06_2_16;
	label$380ee06_2_26:
	return r3;
}

void haxe_io_Output_flush(haxe__io__Output r0) {
	return;
}

void haxe_io_Output_close(haxe__io__Output r0) {
	return;
}

bool haxe_io_Output_set_bigEndian(haxe__io__Output r0,bool r1) {
	r0->bigEndian = r1;
	return r1;
}

void haxe_io_Output_write(haxe__io__Output r0,haxe__io__Bytes r1) {
	venum *r8;
	int r2, r4, r5, r6, r7;
	if( r1 == NULL ) hl_null_access();
	r2 = r1->length;
	r4 = 0;
	label$380ee06_6_3:
	r6 = 0;
	if( r6 >= r2 ) goto label$380ee06_6_16;
	r5 = ((int (*)(haxe__io__Output,haxe__io__Bytes,int,int))r0->$type->vobj_proto[1])(r0,r1,r4,r2);
	r7 = 0;
	if( r5 != r7 ) goto label$380ee06_6_11;
	r8 = (venum*)g$haxe_io_Error_Blocked;
	hl_throw((vdynamic*)r8);
	label$380ee06_6_11:
	r6 = r4 + r5;
	r4 = r6;
	r6 = r2 - r5;
	r2 = r6;
	goto label$380ee06_6_3;
	label$380ee06_6_16:
	return;
}

void haxe_io_Output_writeFullBytes(haxe__io__Output r0,haxe__io__Bytes r1,int r2,int r3) {
	int r4, r5;
	label$380ee06_7_0:
	r5 = 0;
	if( r5 >= r3 ) goto label$380ee06_7_9;
	r4 = ((int (*)(haxe__io__Output,haxe__io__Bytes,int,int))r0->$type->vobj_proto[1])(r0,r1,r2,r3);
	r5 = r2 + r4;
	r2 = r5;
	r5 = r3 - r4;
	r3 = r5;
	goto label$380ee06_7_0;
	label$380ee06_7_9:
	return;
}

void haxe_io_Output_writeFloat(haxe__io__Output r0,double r1) {
	float r4;
	int r3;
	r4 = (float)r1;
	r3 = haxe_io_FPHelper_floatToI32(r4);
	haxe_io_Output_writeInt32(r0,r3);
	return;
}

void haxe_io_Output_writeInt16(haxe__io__Output r0,int r1) {
	venum *r5;
	int r3, r4;
	r4 = -32768;
	if( r1 < r4 ) goto label$380ee06_9_4;
	r4 = 32768;
	if( r1 < r4 ) goto label$380ee06_9_6;
	label$380ee06_9_4:
	r5 = (venum*)g$haxe_io_Error_Overflow;
	hl_throw((vdynamic*)r5);
	label$380ee06_9_6:
	r4 = 65535;
	r3 = r1 & r4;
	haxe_io_Output_writeUInt16(r0,r3);
	return;
}

void haxe_io_Output_writeUInt16(haxe__io__Output r0,int r1) {
	venum *r5;
	bool r6;
	int r3, r4;
	r4 = 0;
	if( r1 < r4 ) goto label$380ee06_10_4;
	r4 = 65536;
	if( r1 < r4 ) goto label$380ee06_10_6;
	label$380ee06_10_4:
	r5 = (venum*)g$haxe_io_Error_Overflow;
	hl_throw((vdynamic*)r5);
	label$380ee06_10_6:
	r6 = r0->bigEndian;
	if( !r6 ) goto label$380ee06_10_15;
	r4 = 8;
	r3 = r1 >> r4;
	((void (*)(haxe__io__Output,int))r0->$type->vobj_proto[0])(r0,r3);
	r4 = 255;
	r3 = r1 & r4;
	((void (*)(haxe__io__Output,int))r0->$type->vobj_proto[0])(r0,r3);
	goto label$380ee06_10_21;
	label$380ee06_10_15:
	r4 = 255;
	r3 = r1 & r4;
	((void (*)(haxe__io__Output,int))r0->$type->vobj_proto[0])(r0,r3);
	r4 = 8;
	r3 = r1 >> r4;
	((void (*)(haxe__io__Output,int))r0->$type->vobj_proto[0])(r0,r3);
	label$380ee06_10_21:
	return;
}

void haxe_io_Output_writeInt32(haxe__io__Output r0,int r1) {
	bool r3;
	int r4, r5;
	r3 = r0->bigEndian;
	if( !r3 ) goto label$380ee06_11_19;
	r5 = 24;
	r4 = ((unsigned)r1) >> r5;
	((void (*)(haxe__io__Output,int))r0->$type->vobj_proto[0])(r0,r4);
	r5 = 16;
	r4 = r1 >> r5;
	r5 = 255;
	r4 = r4 & r5;
	((void (*)(haxe__io__Output,int))r0->$type->vobj_proto[0])(r0,r4);
	r5 = 8;
	r4 = r1 >> r5;
	r5 = 255;
	r4 = r4 & r5;
	((void (*)(haxe__io__Output,int))r0->$type->vobj_proto[0])(r0,r4);
	r5 = 255;
	r4 = r1 & r5;
	((void (*)(haxe__io__Output,int))r0->$type->vobj_proto[0])(r0,r4);
	goto label$380ee06_11_35;
	label$380ee06_11_19:
	r5 = 255;
	r4 = r1 & r5;
	((void (*)(haxe__io__Output,int))r0->$type->vobj_proto[0])(r0,r4);
	r5 = 8;
	r4 = r1 >> r5;
	r5 = 255;
	r4 = r4 & r5;
	((void (*)(haxe__io__Output,int))r0->$type->vobj_proto[0])(r0,r4);
	r5 = 16;
	r4 = r1 >> r5;
	r5 = 255;
	r4 = r4 & r5;
	((void (*)(haxe__io__Output,int))r0->$type->vobj_proto[0])(r0,r4);
	r5 = 24;
	r4 = ((unsigned)r1) >> r5;
	((void (*)(haxe__io__Output,int))r0->$type->vobj_proto[0])(r0,r4);
	label$380ee06_11_35:
	return;
}

void haxe_io_Output_writeString(haxe__io__Output r0,String r1,venum* r2) {
	haxe__io__Bytes r3;
	int r5, r6;
	r3 = haxe_io_Bytes_ofString(r1,r2);
	r5 = 0;
	if( r3 == NULL ) hl_null_access();
	r6 = r3->length;
	haxe_io_Output_writeFullBytes(r0,r3,r5,r6);
	return;
}

