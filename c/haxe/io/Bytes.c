﻿// Generated by HLC 4.0.5 (HL v4)
#define HLC_BOOT
#include <hlc.h>
#include <haxe/io/Bytes.h>
#include <haxe/io/Error.h>
extern venum* g$haxe_io_Error_OutsideBounds;
#include <hl/natives.h>
extern hl_type t$haxe_io_Bytes;
vbyte* hl__Bytes_Bytes_Impl__sub(vbyte*,int,int);
#include <haxe/_Int64/___Int64.h>
extern hl_type t$haxe__Int64____Int64;
void haxe__Int64____Int64_new(haxe___Int64_____Int64,int,int);
extern venum* g$haxe_io_Encoding_RawNative;
extern hl_type t$String;
String String_fromUTF8(vbyte*);
String haxe_io_Bytes_toString(haxe__io__Bytes);
#include <_std/StringBuf.h>
#include <hl/types/ArrayObj.h>
extern hl_type t$StringBuf;
void StringBuf_new(StringBuf);
extern hl_type t$nul_i32;
hl__types__ArrayObj hl_types_ArrayObj_alloc(varray*);
extern String s$0123456789abcdef;
vdynamic* String_charCodeAt(String,int);
int hl_types_ArrayObj_push(hl__types__ArrayObj,vdynamic*);
void StringBuf_addChar(StringBuf,int);
String StringBuf_toString(StringBuf);
extern venum* g$haxe_io_Encoding_UTF8;

void haxe_io_Bytes_new(haxe__io__Bytes r0,vbyte* r1,int r2) {
	r0->b = r1;
	r0->length = r2;
	return;
}

int haxe_io_Bytes_get(haxe__io__Bytes r0,int r1) {
	bool r2;
	vbyte *r5;
	int r3, r4;
	r4 = r0->length;
	if( ((unsigned)r1) >= ((unsigned)r4) ) goto label$f9d86ae_2_4;
	r2 = false;
	goto label$f9d86ae_2_5;
	label$f9d86ae_2_4:
	r2 = true;
	label$f9d86ae_2_5:
	if( !r2 ) goto label$f9d86ae_2_8;
	r3 = 0;
	return r3;
	label$f9d86ae_2_8:
	r5 = r0->b;
	r3 = *(unsigned char*)(r5 + r1);
	return r3;
}

void haxe_io_Bytes_set(haxe__io__Bytes r0,int r1,int r2) {
	venum *r6;
	bool r4;
	vbyte *r7;
	int r5;
	r5 = r0->length;
	if( ((unsigned)r1) >= ((unsigned)r5) ) goto label$f9d86ae_3_4;
	r4 = false;
	goto label$f9d86ae_3_5;
	label$f9d86ae_3_4:
	r4 = true;
	label$f9d86ae_3_5:
	if( !r4 ) goto label$f9d86ae_3_8;
	r6 = (venum*)g$haxe_io_Error_OutsideBounds;
	hl_throw((vdynamic*)r6);
	label$f9d86ae_3_8:
	r7 = r0->b;
	*(unsigned char*)(r7 + r1) = (unsigned char)r2;
	return;
}

void haxe_io_Bytes_blit(haxe__io__Bytes r0,int r1,haxe__io__Bytes r2,int r3,int r4) {
	venum *r9;
	bool r8;
	vbyte *r10, *r11;
	int r6, r7;
	r7 = 0;
	if( r1 < r7 ) goto label$f9d86ae_4_23;
	r7 = 0;
	if( r4 < r7 ) goto label$f9d86ae_4_23;
	r6 = r1 + r4;
	r7 = r0->length;
	if( ((unsigned)r7) < ((unsigned)r6) ) goto label$f9d86ae_4_9;
	r8 = false;
	goto label$f9d86ae_4_10;
	label$f9d86ae_4_9:
	r8 = true;
	label$f9d86ae_4_10:
	if( r8 ) goto label$f9d86ae_4_23;
	r7 = 0;
	if( r3 < r7 ) goto label$f9d86ae_4_23;
	r7 = 0;
	if( r4 < r7 ) goto label$f9d86ae_4_23;
	r6 = r3 + r4;
	if( r2 == NULL ) hl_null_access();
	r7 = r2->length;
	if( ((unsigned)r7) < ((unsigned)r6) ) goto label$f9d86ae_4_21;
	r8 = false;
	goto label$f9d86ae_4_22;
	label$f9d86ae_4_21:
	r8 = true;
	label$f9d86ae_4_22:
	if( !r8 ) goto label$f9d86ae_4_25;
	label$f9d86ae_4_23:
	r9 = (venum*)g$haxe_io_Error_OutsideBounds;
	hl_throw((vdynamic*)r9);
	label$f9d86ae_4_25:
	r10 = r0->b;
	r11 = r2->b;
	hl_bytes_blit(r10,r1,r11,r3,r4);
	return;
}

void haxe_io_Bytes_fill(haxe__io__Bytes r0,int r1,int r2,int r3) {
	venum *r8;
	bool r7;
	vbyte *r9;
	int r5, r6;
	r6 = 0;
	if( r1 < r6 ) goto label$f9d86ae_5_11;
	r6 = 0;
	if( r2 < r6 ) goto label$f9d86ae_5_11;
	r5 = r1 + r2;
	r6 = r0->length;
	if( ((unsigned)r6) < ((unsigned)r5) ) goto label$f9d86ae_5_9;
	r7 = false;
	goto label$f9d86ae_5_10;
	label$f9d86ae_5_9:
	r7 = true;
	label$f9d86ae_5_10:
	if( !r7 ) goto label$f9d86ae_5_13;
	label$f9d86ae_5_11:
	r8 = (venum*)g$haxe_io_Error_OutsideBounds;
	hl_throw((vdynamic*)r8);
	label$f9d86ae_5_13:
	r9 = r0->b;
	hl_bytes_fill(r9,r1,r2,r3);
	return;
}

haxe__io__Bytes haxe_io_Bytes_sub(haxe__io__Bytes r0,int r1,int r2) {
	venum *r7;
	haxe__io__Bytes r8;
	bool r6;
	vbyte *r9;
	int r4, r5;
	r5 = 0;
	if( r1 < r5 ) goto label$f9d86ae_6_11;
	r5 = 0;
	if( r2 < r5 ) goto label$f9d86ae_6_11;
	r4 = r1 + r2;
	r5 = r0->length;
	if( ((unsigned)r5) < ((unsigned)r4) ) goto label$f9d86ae_6_9;
	r6 = false;
	goto label$f9d86ae_6_10;
	label$f9d86ae_6_9:
	r6 = true;
	label$f9d86ae_6_10:
	if( !r6 ) goto label$f9d86ae_6_13;
	label$f9d86ae_6_11:
	r7 = (venum*)g$haxe_io_Error_OutsideBounds;
	hl_throw((vdynamic*)r7);
	label$f9d86ae_6_13:
	r8 = (haxe__io__Bytes)hl_alloc_obj(&t$haxe_io_Bytes);
	r9 = r0->b;
	r9 = hl__Bytes_Bytes_Impl__sub(r9,r1,r2);
	haxe_io_Bytes_new(r8,r9,r2);
	return r8;
}

double haxe_io_Bytes_getDouble(haxe__io__Bytes r0,int r1) {
	bool r2;
	double r5;
	vbyte *r6;
	int r3, r4;
	r4 = 7;
	r3 = r1 + r4;
	r4 = r0->length;
	if( ((unsigned)r3) >= ((unsigned)r4) ) goto label$f9d86ae_7_6;
	r2 = false;
	goto label$f9d86ae_7_7;
	label$f9d86ae_7_6:
	r2 = true;
	label$f9d86ae_7_7:
	if( !r2 ) goto label$f9d86ae_7_10;
	r5 = 0.;
	return r5;
	label$f9d86ae_7_10:
	r6 = r0->b;
	r5 = *(double*)(r6 + r1);
	return r5;
}

double haxe_io_Bytes_getFloat(haxe__io__Bytes r0,int r1) {
	bool r2;
	float r7;
	double r5;
	vbyte *r6;
	int r3, r4;
	r4 = 3;
	r3 = r1 + r4;
	r4 = r0->length;
	if( ((unsigned)r3) >= ((unsigned)r4) ) goto label$f9d86ae_8_6;
	r2 = false;
	goto label$f9d86ae_8_7;
	label$f9d86ae_8_6:
	r2 = true;
	label$f9d86ae_8_7:
	if( !r2 ) goto label$f9d86ae_8_10;
	r5 = 0.;
	return r5;
	label$f9d86ae_8_10:
	r6 = r0->b;
	r7 = *(float*)(r6 + r1);
	r5 = (double)r7;
	return r5;
}

void haxe_io_Bytes_setFloat(haxe__io__Bytes r0,int r1,double r2) {
	venum *r7;
	bool r4;
	float r9;
	vbyte *r8;
	int r5, r6;
	r6 = 3;
	r5 = r1 + r6;
	r6 = r0->length;
	if( ((unsigned)r5) >= ((unsigned)r6) ) goto label$f9d86ae_9_6;
	r4 = false;
	goto label$f9d86ae_9_7;
	label$f9d86ae_9_6:
	r4 = true;
	label$f9d86ae_9_7:
	if( !r4 ) goto label$f9d86ae_9_10;
	r7 = (venum*)g$haxe_io_Error_OutsideBounds;
	hl_throw((vdynamic*)r7);
	label$f9d86ae_9_10:
	r8 = r0->b;
	r9 = (float)r2;
	*(float*)(r8 + r1) = r9;
	return;
}

int haxe_io_Bytes_getInt32(haxe__io__Bytes r0,int r1) {
	bool r2;
	vbyte *r5;
	int r3, r4;
	r4 = 3;
	r3 = r1 + r4;
	r4 = r0->length;
	if( ((unsigned)r3) >= ((unsigned)r4) ) goto label$f9d86ae_10_6;
	r2 = false;
	goto label$f9d86ae_10_7;
	label$f9d86ae_10_6:
	r2 = true;
	label$f9d86ae_10_7:
	if( !r2 ) goto label$f9d86ae_10_10;
	r3 = 0;
	return r3;
	label$f9d86ae_10_10:
	r5 = r0->b;
	r3 = *(int*)(r5 + r1);
	return r3;
}

haxe___Int64_____Int64 haxe_io_Bytes_getInt64(haxe__io__Bytes r0,int r1) {
	bool r3;
	haxe___Int64_____Int64 r6;
	vbyte *r7, *r8;
	int r4, r5, r9;
	r5 = 7;
	r4 = r1 + r5;
	r5 = r0->length;
	if( ((unsigned)r4) >= ((unsigned)r5) ) goto label$f9d86ae_11_6;
	r3 = false;
	goto label$f9d86ae_11_7;
	label$f9d86ae_11_6:
	r3 = true;
	label$f9d86ae_11_7:
	if( !r3 ) goto label$f9d86ae_11_13;
	r6 = (haxe___Int64_____Int64)hl_alloc_obj(&t$haxe__Int64____Int64);
	r4 = 0;
	r5 = 0;
	haxe__Int64____Int64_new(r6,r4,r5);
	return r6;
	label$f9d86ae_11_13:
	r7 = r0->b;
	r5 = 4;
	r4 = r1 + r5;
	r4 = *(int*)(r7 + r4);
	r8 = r0->b;
	r6 = (haxe___Int64_____Int64)hl_alloc_obj(&t$haxe__Int64____Int64);
	r9 = *(int*)(r8 + r1);
	haxe__Int64____Int64_new(r6,r4,r9);
	return r6;
}

void haxe_io_Bytes_setInt32(haxe__io__Bytes r0,int r1,int r2) {
	venum *r7;
	bool r4;
	vbyte *r8;
	int r5, r6;
	r6 = 3;
	r5 = r1 + r6;
	r6 = r0->length;
	if( ((unsigned)r5) >= ((unsigned)r6) ) goto label$f9d86ae_12_6;
	r4 = false;
	goto label$f9d86ae_12_7;
	label$f9d86ae_12_6:
	r4 = true;
	label$f9d86ae_12_7:
	if( !r4 ) goto label$f9d86ae_12_10;
	r7 = (venum*)g$haxe_io_Error_OutsideBounds;
	hl_throw((vdynamic*)r7);
	label$f9d86ae_12_10:
	r8 = r0->b;
	*(int*)(r8 + r1) = r2;
	return;
}

String haxe_io_Bytes_getString(haxe__io__Bytes r0,int r1,int r2,venum* r3) {
	String r12;
	venum *r8, *r11;
	bool r7;
	vbyte *r9, *r10;
	int r5, r6;
	r6 = 0;
	if( r1 < r6 ) goto label$f9d86ae_13_11;
	r6 = 0;
	if( r2 < r6 ) goto label$f9d86ae_13_11;
	r5 = r1 + r2;
	r6 = r0->length;
	if( ((unsigned)r6) < ((unsigned)r5) ) goto label$f9d86ae_13_9;
	r7 = false;
	goto label$f9d86ae_13_10;
	label$f9d86ae_13_9:
	r7 = true;
	label$f9d86ae_13_10:
	if( !r7 ) goto label$f9d86ae_13_13;
	label$f9d86ae_13_11:
	r8 = (venum*)g$haxe_io_Error_OutsideBounds;
	hl_throw((vdynamic*)r8);
	label$f9d86ae_13_13:
	r6 = 2;
	r5 = r2 + r6;
	r9 = hl_alloc_bytes(r5);
	r5 = 0;
	r10 = r0->b;
	hl_bytes_blit(r9,r5,r10,r1,r2);
	r6 = 0;
	*(unsigned char*)(r9 + r2) = (unsigned char)r6;
	r6 = 1;
	r5 = r2 + r6;
	r6 = 0;
	*(unsigned char*)(r9 + r5) = (unsigned char)r6;
	r11 = (venum*)g$haxe_io_Encoding_RawNative;
	if( r3 != r11 ) goto label$f9d86ae_13_33;
	r12 = (String)hl_alloc_obj(&t$String);
	r12->bytes = r9;
	r5 = 0;
	r5 = hl_ucs2length(r9,r5);
	r12->length = r5;
	return r12;
	label$f9d86ae_13_33:
	r12 = String_fromUTF8(r9);
	return r12;
}

vbyte* haxe_io_Bytes___string(haxe__io__Bytes r0) {
	String r2;
	vbyte *r1;
	r2 = haxe_io_Bytes_toString(r0);
	if( r2 == NULL ) hl_null_access();
	r1 = r2->bytes;
	return r1;
}

String haxe_io_Bytes_toString(haxe__io__Bytes r0) {
	String r1;
	venum *r4;
	int r2, r3;
	r2 = 0;
	r3 = r0->length;
	r4 = NULL;
	r1 = haxe_io_Bytes_getString(r0,r2,r3,r4);
	return r1;
}

String haxe_io_Bytes_toHex(haxe__io__Bytes r0) {
	String r7, r9;
	hl__types__ArrayObj r3;
	hl_type *r5;
	StringBuf r1;
	vdynamic *r12, *r15;
	int r6, r8, r10, r11, r13, r14;
	varray *r4;
	r1 = (StringBuf)hl_alloc_obj(&t$StringBuf);
	StringBuf_new(r1);
	r5 = &t$nul_i32;
	r6 = 0;
	r4 = hl_alloc_array(r5,r6);
	r3 = hl_types_ArrayObj_alloc(r4);
	r7 = (String)s$0123456789abcdef;
	r6 = 0;
	if( r7 == NULL ) hl_null_access();
	r8 = r7->length;
	label$f9d86ae_16_10:
	if( r6 >= r8 ) goto label$f9d86ae_16_19;
	r10 = r6;
	++r6;
	if( r3 == NULL ) hl_null_access();
	if( r7 == NULL ) hl_null_access();
	r12 = String_charCodeAt(r7,r10);
	r11 = hl_types_ArrayObj_push(r3,((vdynamic*)r12));
	goto label$f9d86ae_16_10;
	label$f9d86ae_16_19:
	r6 = 0;
	r8 = r0->length;
	label$f9d86ae_16_21:
	if( r6 >= r8 ) goto label$f9d86ae_16_51;
	r10 = r6;
	++r6;
	r11 = haxe_io_Bytes_get(r0,r10);
	if( r1 == NULL ) hl_null_access();
	if( r3 == NULL ) hl_null_access();
	r14 = 4;
	r13 = r11 >> r14;
	r14 = r3->length;
	if( ((unsigned)r13) < ((unsigned)r14) ) goto label$f9d86ae_16_34;
	r12 = NULL;
	goto label$f9d86ae_16_37;
	label$f9d86ae_16_34:
	r4 = r3->array;
	r15 = ((vdynamic**)(r4 + 1))[r13];
	r12 = (vdynamic*)r15;
	label$f9d86ae_16_37:
	r13 = r12 ? r12->v.i : 0;
	StringBuf_addChar(r1,r13);
	r14 = 15;
	r13 = r11 & r14;
	r14 = r3->length;
	if( ((unsigned)r13) < ((unsigned)r14) ) goto label$f9d86ae_16_45;
	r12 = NULL;
	goto label$f9d86ae_16_48;
	label$f9d86ae_16_45:
	r4 = r3->array;
	r15 = ((vdynamic**)(r4 + 1))[r13];
	r12 = (vdynamic*)r15;
	label$f9d86ae_16_48:
	r13 = r12 ? r12->v.i : 0;
	StringBuf_addChar(r1,r13);
	goto label$f9d86ae_16_21;
	label$f9d86ae_16_51:
	if( r1 == NULL ) hl_null_access();
	r9 = StringBuf_toString(r1);
	return r9;
}

haxe__io__Bytes haxe_io_Bytes_alloc(int r0) {
	haxe__io__Bytes r5;
	int r2, r4;
	vbyte *r1;
	r1 = hl_alloc_bytes(r0);
	r2 = 0;
	r4 = 0;
	hl_bytes_fill(r1,r2,r0,r4);
	r5 = (haxe__io__Bytes)hl_alloc_obj(&t$haxe_io_Bytes);
	haxe_io_Bytes_new(r5,r1,r0);
	return r5;
}

haxe__io__Bytes haxe_io_Bytes_ofString(String r0,venum* r1) {
	venum *r3;
	haxe__io__Bytes r10;
	int *r7;
	vbyte *r5, *r8;
	int r4, r6, r9, r11;
	if( r1 ) goto label$f9d86ae_18_3;
	r3 = (venum*)g$haxe_io_Encoding_UTF8;
	r1 = r3;
	label$f9d86ae_18_3:
	if( r1 == NULL ) hl_null_access();
	r4 = HL__ENUM_INDEX__(r1);
	switch(r4) {
		default:
			goto label$f9d86ae_18_29;
		case 0:
			r4 = 0;
			if( r0 == NULL ) hl_null_access();
			r5 = r0->bytes;
			r6 = r0->length;
			r7 = &r4;
			r8 = hl_utf16_to_utf8(r5,r6,r7);
			r10 = (haxe__io__Bytes)hl_alloc_obj(&t$haxe_io_Bytes);
			haxe_io_Bytes_new(r10,r8,r4);
			return r10;
		case 1:
			r10 = (haxe__io__Bytes)hl_alloc_obj(&t$haxe_io_Bytes);
			if( r0 == NULL ) hl_null_access();
			r5 = r0->bytes;
			r6 = 0;
			r9 = r0->length;
			r11 = 1;
			r9 = r9 << r11;
			r5 = hl__Bytes_Bytes_Impl__sub(r5,r6,r9);
			r6 = r0->length;
			r9 = 1;
			r6 = r6 << r9;
			haxe_io_Bytes_new(r10,r5,r6);
			return r10;
	}
	label$f9d86ae_18_29:
	r10 = NULL;
	return r10;
}

