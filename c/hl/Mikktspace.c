﻿// Generated by HLC 4.0.5 (HL v4)
#define HLC_BOOT
#include <hlc.h>
#include <hl/Mikktspace.h>
#include <hl/natives.h>
extern String s$assert;

void hl_Mikktspace_new(hl__Mikktspace r0) {
	return;
}

void hl_Mikktspace_compute(hl__Mikktspace r0,double* r1) {
	String r5;
	bool r4;
	double r2;
	if( r1 ) goto label$3ac464f_2_3;
	r2 = 180.;
	goto label$3ac464f_2_4;
	label$3ac464f_2_3:
	r2 = *r1;
	label$3ac464f_2_4:
	r4 = fmt_compute_mikkt_tangents(((vdynamic*)r0),r2);
	if( r4 ) goto label$3ac464f_2_8;
	r5 = (String)s$assert;
	hl_throw((vdynamic*)r5);
	label$3ac464f_2_8:
	return;
}

