﻿// Generated by HLC 4.0.5 (HL v4)
#define HLC_BOOT
#include <hlc.h>
#include <hxd/snd/Data.h>
#include <haxe/io/Bytes.h>
int hxd_snd_Data_getBytesPerSample(hxd__snd__Data);
extern String s$sampleStart_;
#include <hl/natives.h>
String String___alloc__(vbyte*,int);
String String___add__(String,String);
extern String s$_sampleCount_;
extern String s$_outPos_;
extern String s$_bpp_;
extern String s$_out_length_;
void haxe_io_Bytes_fill(haxe__io__Bytes,int,int,int);
#include <hxd/snd/WavData.h>
haxe__io__Bytes haxe_io_Bytes_alloc(int);
void hxd_snd_Data_resampleBuffer(hxd__snd__Data,haxe__io__Bytes,int,haxe__io__Bytes,int,int,venum*,int,int);
extern hl_type t$hxd_snd_WavData;
void hxd_snd_WavData_new(hxd__snd__WavData,haxe__io__Bytes);
#include <haxe/io/Error.h>
extern venum* g$hxd_snd_SampleFormat_I16;
extern venum* g$haxe_io_Error_OutsideBounds;
int haxe_io_Bytes_get(haxe__io__Bytes,int);
double haxe_io_Bytes_getFloat(haxe__io__Bytes,int);
void haxe_io_Bytes_set(haxe__io__Bytes,int,int);
void haxe_io_Bytes_setFloat(haxe__io__Bytes,int,double);
extern String s$Not_implemented;

bool hxd_snd_Data_isLoading(hxd__snd__Data r0) {
	bool r1;
	r1 = false;
	return r1;
}

void hxd_snd_Data_decode(hxd__snd__Data r0,haxe__io__Bytes r1,int r2,int r3,int r4) {
	String r10, r13, r14, r15;
	int *r11;
	vbyte *r12;
	int r5, r7, r8, r9, r16;
	r5 = hxd_snd_Data_getBytesPerSample(r0);
	r8 = 0;
	if( r3 < r8 ) goto label$0e435a3_2_12;
	r8 = 0;
	if( r4 < r8 ) goto label$0e435a3_2_12;
	r8 = 0;
	if( r2 < r8 ) goto label$0e435a3_2_12;
	r8 = r4 * r5;
	r7 = r2 + r8;
	if( r1 == NULL ) hl_null_access();
	r8 = r1->length;
	if( r8 >= r7 ) goto label$0e435a3_2_48;
	label$0e435a3_2_12:
	r10 = (String)s$sampleStart_;
	r7 = r3;
	r11 = &r7;
	r12 = hl_itos(r7,r11);
	r13 = String___alloc__(r12,r7);
	r10 = String___add__(r10,r13);
	r14 = (String)s$_sampleCount_;
	r7 = r4;
	r11 = &r7;
	r12 = hl_itos(r7,r11);
	r15 = String___alloc__(r12,r7);
	r14 = String___add__(r14,r15);
	r13 = String___add__(r10,r14);
	r14 = (String)s$_outPos_;
	r7 = r2;
	r11 = &r7;
	r12 = hl_itos(r7,r11);
	r15 = String___alloc__(r12,r7);
	r14 = String___add__(r14,r15);
	r13 = String___add__(r13,r14);
	r14 = (String)s$_bpp_;
	r7 = r5;
	r11 = &r7;
	r12 = hl_itos(r7,r11);
	r15 = String___alloc__(r12,r7);
	r14 = String___add__(r14,r15);
	r13 = String___add__(r13,r14);
	r14 = (String)s$_out_length_;
	if( r1 == NULL ) hl_null_access();
	r7 = r1->length;
	r11 = &r7;
	r12 = hl_itos(r7,r11);
	r15 = String___alloc__(r12,r7);
	r14 = String___add__(r14,r15);
	r13 = String___add__(r13,r14);
	hl_throw((vdynamic*)r13);
	label$0e435a3_2_48:
	r7 = r3 + r4;
	r8 = r0->samples;
	if( r7 < r8 ) goto label$0e435a3_2_65;
	r7 = 0;
	r9 = r0->samples;
	if( r3 >= r9 ) goto label$0e435a3_2_58;
	r8 = r0->samples;
	r8 = r8 - r3;
	r7 = r8;
	((void (*)(hxd__snd__Data,haxe__io__Bytes,int,int,int))r0->$type->vobj_proto[1])(r0,r1,r2,r3,r7);
	label$0e435a3_2_58:
	r9 = r7 * r5;
	r8 = r2 + r9;
	r9 = r4 - r7;
	r9 = r9 * r5;
	r16 = 0;
	haxe_io_Bytes_fill(r1,r8,r9,r16);
	return;
	label$0e435a3_2_65:
	((void (*)(hxd__snd__Data,haxe__io__Bytes,int,int,int))r0->$type->vobj_proto[1])(r0,r1,r2,r3,r4);
	return;
}

hxd__snd__Data hxd_snd_Data_resample(hxd__snd__Data r0,int r1,venum* r2,int r3) {
	hxd__snd__WavData r19;
	venum *r5;
	haxe__io__Bytes r11, r14, r17;
	double r8, r9, r10;
	int r6, r7, r12, r13, r15, r16, r18;
	r5 = r0->sampleFormat;
	if( r5 != r2 ) goto label$0e435a3_3_7;
	r6 = r0->samplingRate;
	if( r6 != r1 ) goto label$0e435a3_3_7;
	r6 = r0->channels;
	if( r6 != r3 ) goto label$0e435a3_3_7;
	return r0;
	label$0e435a3_3_7:
	r6 = r0->samples;
	r8 = (double)r6;
	r9 = (double)r1;
	r6 = r0->samplingRate;
	r10 = (double)r6;
	r9 = r9 / r10;
	r8 = r8 * r9;
	r6 = hl_math_ceil(r8);
	r7 = hxd_snd_Data_getBytesPerSample(r0);
	r13 = r0->samples;
	r12 = r7 * r13;
	r11 = haxe_io_Bytes_alloc(r12);
	r12 = 0;
	r13 = 0;
	r15 = r0->samples;
	((void (*)(hxd__snd__Data,haxe__io__Bytes,int,int,int))r0->$type->vobj_proto[1])(r0,r11,r12,r13,r15);
	r12 = r3 * r6;
	if( r2 == NULL ) hl_null_access();
	r13 = HL__ENUM_INDEX__(r2);
	switch(r13) {
		default:
			goto label$0e435a3_3_36;
		case 0:
			r15 = 1;
			r13 = r15;
			goto label$0e435a3_3_36;
		case 1:
			r15 = 2;
			r13 = r15;
			goto label$0e435a3_3_36;
		case 2:
			r15 = 4;
			r13 = r15;
	}
	label$0e435a3_3_36:
	r15 = r12 * r13;
	r14 = haxe_io_Bytes_alloc(r15);
	r15 = 0;
	r16 = 0;
	r18 = r0->samples;
	hxd_snd_Data_resampleBuffer(r0,r14,r15,r11,r16,r1,r2,r3,r18);
	r19 = (hxd__snd__WavData)hl_alloc_obj(&t$hxd_snd_WavData);
	r17 = NULL;
	hxd_snd_WavData_new(r19,r17);
	r19->channels = r3;
	r19->samples = r6;
	r19->sampleFormat = r2;
	r19->samplingRate = r1;
	r19->rawData = r14;
	return ((hxd__snd__Data)r19);
}

void hxd_snd_Data_resampleBuffer(hxd__snd__Data r0,haxe__io__Bytes r1,int r2,haxe__io__Bytes r3,int r4,int r5,venum* r6,int r7,int r8) {
	venum *r19, *r20, *r30;
	bool r15, r18;
	double r12, r13, r14, r31, r35, r36, r37, r42;
	vbyte *r26;
	int r9, r11, r16, r17, r21, r22, r23, r24, r25, r27, r28, r29, r32, r33, r34, r38, r39, r40, r41;
	r9 = hxd_snd_Data_getBytesPerSample(r0);
	r12 = (double)r8;
	r13 = (double)r5;
	r11 = r0->samplingRate;
	r14 = (double)r11;
	r13 = r13 / r14;
	r12 = r12 * r13;
	r11 = hl_math_ceil(r12);
	if( r8 != r11 ) goto label$0e435a3_4_11;
	r15 = false;
	goto label$0e435a3_4_12;
	label$0e435a3_4_11:
	r15 = true;
	label$0e435a3_4_12:
	if( r15 ) goto label$0e435a3_4_113;
	r19 = r0->sampleFormat;
	r20 = (venum*)g$hxd_snd_SampleFormat_I16;
	if( r19 != r20 ) goto label$0e435a3_4_113;
	r20 = (venum*)g$hxd_snd_SampleFormat_I16;
	if( r6 != r20 ) goto label$0e435a3_4_113;
	r17 = 1;
	if( r7 != r17 ) goto label$0e435a3_4_113;
	r16 = r0->channels;
	r17 = 2;
	if( r16 != r17 ) goto label$0e435a3_4_113;
	r16 = r4;
	r17 = r2;
	r21 = 0;
	r22 = r8;
	label$0e435a3_4_27:
	if( r21 >= r22 ) goto label$0e435a3_4_112;
	++r21;
	r25 = 1;
	r24 = r16 + r25;
	if( r3 == NULL ) hl_null_access();
	r25 = r3->length;
	if( ((unsigned)r24) >= ((unsigned)r25) ) goto label$0e435a3_4_37;
	r18 = false;
	goto label$0e435a3_4_38;
	label$0e435a3_4_37:
	r18 = true;
	label$0e435a3_4_38:
	if( !r18 ) goto label$0e435a3_4_42;
	r25 = 0;
	r24 = r25;
	goto label$0e435a3_4_45;
	label$0e435a3_4_42:
	r26 = r3->b;
	r25 = *(unsigned short*)(r26 + r16);
	r24 = r25;
	label$0e435a3_4_45:
	r27 = 2;
	r25 = r16 + r27;
	r16 = r25;
	r27 = 1;
	r25 = r25 + r27;
	r27 = r3->length;
	if( ((unsigned)r25) >= ((unsigned)r27) ) goto label$0e435a3_4_54;
	r18 = false;
	goto label$0e435a3_4_55;
	label$0e435a3_4_54:
	r18 = true;
	label$0e435a3_4_55:
	if( !r18 ) goto label$0e435a3_4_59;
	r27 = 0;
	r25 = r27;
	goto label$0e435a3_4_62;
	label$0e435a3_4_59:
	r26 = r3->b;
	r27 = *(unsigned short*)(r26 + r16);
	r25 = r27;
	label$0e435a3_4_62:
	r28 = 2;
	r27 = r16 + r28;
	r16 = r27;
	r27 = r24 ^ r25;
	r28 = 32768;
	if( r27 < r28 ) goto label$0e435a3_4_91;
	r28 = 32768;
	r27 = r24 & r28;
	r28 = 0;
	if( r27 != r28 ) goto label$0e435a3_4_73;
	goto label$0e435a3_4_76;
	label$0e435a3_4_73:
	r28 = -65536;
	r27 = r24 | r28;
	r24 = r27;
	label$0e435a3_4_76:
	r28 = 32768;
	r27 = r25 & r28;
	r28 = 0;
	if( r27 != r28 ) goto label$0e435a3_4_81;
	goto label$0e435a3_4_84;
	label$0e435a3_4_81:
	r28 = -65536;
	r27 = r25 | r28;
	r25 = r27;
	label$0e435a3_4_84:
	r28 = r24 + r25;
	r29 = 1;
	r28 = r28 >> r29;
	r29 = 65535;
	r28 = r28 & r29;
	r27 = r28;
	goto label$0e435a3_4_95;
	label$0e435a3_4_91:
	r28 = r24 + r25;
	r29 = 1;
	r28 = r28 >> r29;
	r27 = r28;
	label$0e435a3_4_95:
	r29 = 1;
	r28 = r17 + r29;
	if( r1 == NULL ) hl_null_access();
	r29 = r1->length;
	if( ((unsigned)r28) >= ((unsigned)r29) ) goto label$0e435a3_4_102;
	r18 = false;
	goto label$0e435a3_4_103;
	label$0e435a3_4_102:
	r18 = true;
	label$0e435a3_4_103:
	if( !r18 ) goto label$0e435a3_4_106;
	r30 = (venum*)g$haxe_io_Error_OutsideBounds;
	hl_throw((vdynamic*)r30);
	label$0e435a3_4_106:
	r26 = r1->b;
	*(unsigned short*)(r26 + r17) = (unsigned short)r27;
	r29 = 2;
	r28 = r17 + r29;
	r17 = r28;
	goto label$0e435a3_4_27;
	label$0e435a3_4_112:
	return;
	label$0e435a3_4_113:
	r16 = r0->channels;
	if( r7 >= r16 ) goto label$0e435a3_4_117;
	r17 = r7;
	goto label$0e435a3_4_118;
	label$0e435a3_4_117:
	r17 = r16;
	label$0e435a3_4_118:
	r21 = r7 - r17;
	r12 = 0.;
	r22 = 0;
	r23 = 0;
	r24 = r11;
	label$0e435a3_4_123:
	if( r23 >= r24 ) goto label$0e435a3_4_337;
	r25 = r23;
	++r23;
	r13 = (double)r25;
	r28 = 1;
	r27 = r11 - r28;
	r14 = (double)r27;
	r13 = r13 / r14;
	r28 = 1;
	r27 = r8 - r28;
	r14 = (double)r27;
	r13 = r13 * r14;
	r27 = (int)r13;
	r31 = (double)r27;
	r14 = r13 - r31;
	r29 = r27 * r9;
	r28 = r4 + r29;
	r33 = 1;
	r32 = r8 - r33;
	if( r27 != r32 ) goto label$0e435a3_4_146;
	r18 = false;
	r15 = r18;
	label$0e435a3_4_146:
	r29 = 0;
	r32 = r17;
	label$0e435a3_4_148:
	if( r29 >= r32 ) goto label$0e435a3_4_299;
	++r29;
	r31 = 0.;
	r19 = r0->sampleFormat;
	if( r19 == NULL ) hl_null_access();
	r34 = HL__ENUM_INDEX__(r19);
	switch(r34) {
		default:
			goto label$0e435a3_4_241;
		case 0:
			if( r3 == NULL ) hl_null_access();
			r34 = haxe_io_Bytes_get(r3,r28);
			r36 = (double)r34;
			r37 = 255.;
			r36 = r36 / r37;
			r35 = r36;
			if( !r15 ) goto label$0e435a3_4_170;
			r34 = r28 + r9;
			r34 = haxe_io_Bytes_get(r3,r34);
			r36 = (double)r34;
			r37 = 255.;
			r36 = r36 / r37;
			r31 = r36;
			label$0e435a3_4_170:
			++r28;
			goto label$0e435a3_4_241;
		case 1:
			r38 = 1;
			r34 = r28 + r38;
			if( r3 == NULL ) hl_null_access();
			r38 = r3->length;
			if( ((unsigned)r34) >= ((unsigned)r38) ) goto label$0e435a3_4_179;
			r18 = false;
			goto label$0e435a3_4_180;
			label$0e435a3_4_179:
			r18 = true;
			label$0e435a3_4_180:
			if( !r18 ) goto label$0e435a3_4_184;
			r38 = 0;
			r34 = r38;
			goto label$0e435a3_4_187;
			label$0e435a3_4_184:
			r26 = r3->b;
			r38 = *(unsigned short*)(r26 + r28);
			r34 = r38;
			label$0e435a3_4_187:
			r39 = 32768;
			r38 = r38 & r39;
			r39 = 0;
			if( r38 != r39 ) goto label$0e435a3_4_193;
			r38 = r34;
			goto label$0e435a3_4_195;
			label$0e435a3_4_193:
			r39 = -65536;
			r38 = r34 | r39;
			label$0e435a3_4_195:
			r36 = (double)r38;
			r37 = 32768.;
			r36 = r36 / r37;
			r35 = r36;
			if( !r15 ) goto label$0e435a3_4_227;
			r38 = r28 + r9;
			r40 = 1;
			r39 = r38 + r40;
			r40 = r3->length;
			if( ((unsigned)r39) >= ((unsigned)r40) ) goto label$0e435a3_4_207;
			r18 = false;
			goto label$0e435a3_4_208;
			label$0e435a3_4_207:
			r18 = true;
			label$0e435a3_4_208:
			if( !r18 ) goto label$0e435a3_4_212;
			r40 = 0;
			r39 = r40;
			goto label$0e435a3_4_215;
			label$0e435a3_4_212:
			r26 = r3->b;
			r40 = *(unsigned short*)(r26 + r38);
			r39 = r40;
			label$0e435a3_4_215:
			r41 = 32768;
			r40 = r40 & r41;
			r41 = 0;
			if( r40 != r41 ) goto label$0e435a3_4_221;
			r40 = r39;
			goto label$0e435a3_4_223;
			label$0e435a3_4_221:
			r41 = -65536;
			r40 = r39 | r41;
			label$0e435a3_4_223:
			r36 = (double)r40;
			r37 = 32768.;
			r36 = r36 / r37;
			r31 = r36;
			label$0e435a3_4_227:
			r39 = 2;
			r38 = r28 + r39;
			r28 = r38;
			goto label$0e435a3_4_241;
		case 2:
			if( r3 == NULL ) hl_null_access();
			r36 = haxe_io_Bytes_getFloat(r3,r28);
			r35 = r36;
			if( !r15 ) goto label$0e435a3_4_238;
			r34 = r28 + r9;
			r36 = haxe_io_Bytes_getFloat(r3,r34);
			r31 = r36;
			label$0e435a3_4_238:
			r38 = 4;
			r34 = r28 + r38;
			r28 = r34;
	}
	label$0e435a3_4_241:
	if( !r15 ) goto label$0e435a3_4_246;
	r42 = r31 - r35;
	r37 = r14 * r42;
	r36 = r35 + r37;
	goto label$0e435a3_4_247;
	label$0e435a3_4_246:
	r36 = r35;
	label$0e435a3_4_247:
	r12 = r36;
	if( r6 == NULL ) hl_null_access();
	r34 = HL__ENUM_INDEX__(r6);
	switch(r34) {
		default:
			goto label$0e435a3_4_298;
		case 0:
			r37 = 1.;
			r36 = r36 + r37;
			r37 = 128.;
			r36 = r36 * r37;
			r34 = (int)r36;
			r22 = r34;
			r38 = 255;
			if( r38 >= r34 ) goto label$0e435a3_4_262;
			r34 = 255;
			r22 = r34;
			label$0e435a3_4_262:
			if( r1 == NULL ) hl_null_access();
			r34 = r2;
			++r2;
			haxe_io_Bytes_set(r1,r34,r22);
			goto label$0e435a3_4_298;
		case 1:
			r37 = 32768.;
			r36 = r36 * r37;
			r34 = (int)r36;
			r38 = 32767;
			if( r38 >= r34 ) goto label$0e435a3_4_273;
			r34 = 32767;
			label$0e435a3_4_273:
			r38 = 65535;
			r34 = r34 & r38;
			r22 = r34;
			r38 = 1;
			r34 = r2 + r38;
			if( r1 == NULL ) hl_null_access();
			r38 = r1->length;
			if( ((unsigned)r34) >= ((unsigned)r38) ) goto label$0e435a3_4_283;
			r18 = false;
			goto label$0e435a3_4_284;
			label$0e435a3_4_283:
			r18 = true;
			label$0e435a3_4_284:
			if( !r18 ) goto label$0e435a3_4_287;
			r30 = (venum*)g$haxe_io_Error_OutsideBounds;
			hl_throw((vdynamic*)r30);
			label$0e435a3_4_287:
			r26 = r1->b;
			*(unsigned short*)(r26 + r2) = (unsigned short)r22;
			r38 = 2;
			r34 = r2 + r38;
			r2 = r34;
			goto label$0e435a3_4_298;
		case 2:
			if( r1 == NULL ) hl_null_access();
			haxe_io_Bytes_setFloat(r1,r2,r36);
			r38 = 4;
			r34 = r2 + r38;
			r2 = r34;
	}
	label$0e435a3_4_298:
	goto label$0e435a3_4_148;
	label$0e435a3_4_299:
	r29 = 0;
	r32 = r21;
	label$0e435a3_4_301:
	if( r29 >= r32 ) goto label$0e435a3_4_336;
	++r29;
	if( r6 == NULL ) hl_null_access();
	r34 = HL__ENUM_INDEX__(r6);
	switch(r34) {
		default:
			goto label$0e435a3_4_335;
		case 0:
			if( r1 == NULL ) hl_null_access();
			r34 = r2;
			++r2;
			haxe_io_Bytes_set(r1,r34,r22);
			goto label$0e435a3_4_335;
		case 1:
			r38 = 1;
			r34 = r2 + r38;
			if( r1 == NULL ) hl_null_access();
			r38 = r1->length;
			if( ((unsigned)r34) >= ((unsigned)r38) ) goto label$0e435a3_4_320;
			r18 = false;
			goto label$0e435a3_4_321;
			label$0e435a3_4_320:
			r18 = true;
			label$0e435a3_4_321:
			if( !r18 ) goto label$0e435a3_4_324;
			r30 = (venum*)g$haxe_io_Error_OutsideBounds;
			hl_throw((vdynamic*)r30);
			label$0e435a3_4_324:
			r26 = r1->b;
			*(unsigned short*)(r26 + r2) = (unsigned short)r22;
			r38 = 2;
			r34 = r2 + r38;
			r2 = r34;
			goto label$0e435a3_4_335;
		case 2:
			if( r1 == NULL ) hl_null_access();
			haxe_io_Bytes_setFloat(r1,r2,r12);
			r38 = 4;
			r34 = r2 + r38;
			r2 = r34;
	}
	label$0e435a3_4_335:
	goto label$0e435a3_4_301;
	label$0e435a3_4_336:
	goto label$0e435a3_4_123;
	label$0e435a3_4_337:
	return;
}

void hxd_snd_Data_decodeBuffer(hxd__snd__Data r0,haxe__io__Bytes r1,int r2,int r3,int r4) {
	String r5;
	r5 = (String)s$Not_implemented;
	hl_throw((vdynamic*)r5);
}

int hxd_snd_Data_getBytesPerSample(hxd__snd__Data r0) {
	venum *r2;
	int r1, r3;
	r2 = r0->sampleFormat;
	if( r2 == NULL ) hl_null_access();
	r1 = HL__ENUM_INDEX__(r2);
	switch(r1) {
		default:
			goto label$0e435a3_6_13;
		case 0:
			r3 = 1;
			r1 = r3;
			goto label$0e435a3_6_13;
		case 1:
			r3 = 2;
			r1 = r3;
			goto label$0e435a3_6_13;
		case 2:
			r3 = 4;
			r1 = r3;
	}
	label$0e435a3_6_13:
	r3 = r0->channels;
	r3 = r3 * r1;
	return r3;
}

int hxd_snd_Data_formatBytes(venum* r0) {
	int r1;
	if( r0 == NULL ) hl_null_access();
	r1 = HL__ENUM_INDEX__(r0);
	switch(r1) {
		default:
			goto label$0e435a3_7_10;
		case 0:
			r1 = 1;
			return r1;
		case 1:
			r1 = 2;
			return r1;
		case 2:
			r1 = 4;
			return r1;
	}
	label$0e435a3_7_10:
	r1 = 0;
	return r1;
}

void hxd_snd_Data_load(hxd__snd__Data r0,vclosure* r1) {
	if( r1 == NULL ) hl_null_access();
	r1->hasValue ? ((void (*)(vdynamic*))r1->fun)((vdynamic*)r1->value) : ((void (*)(void))r1->fun)();
	return;
}

double hxd_snd_Data_get_duration(hxd__snd__Data r0) {
	double r1, r3;
	int r2;
	r2 = r0->samples;
	r1 = (double)r2;
	r2 = r0->samplingRate;
	r3 = (double)r2;
	r1 = r1 / r3;
	return r1;
}

