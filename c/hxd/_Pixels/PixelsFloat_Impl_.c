﻿// Generated by HLC 4.0.5 (HL v4)
#define HLC_BOOT
#include <hlc.h>
#include <hxd/_Pixels/PixelsFloat_Impl_.h>
#include <haxe/io/Bytes.h>
extern hl_type t$h3d_Vector;
double haxe_io_Bytes_getFloat(haxe__io__Bytes,int);
void h3d_Vector_new(h3d__Vector,double*,double*,double*,double*);
void haxe_io_Bytes_setFloat(haxe__io__Bytes,int,double);
#include <hxd/PixelFormat.h>
extern venum* g$hxd_PixelFormat_RGBA32F;
void hxd_Pixels_convert(hxd__Pixels,venum*);
void hxd_Pixels_setFlip(hxd__Pixels,bool);

h3d__Vector hxd__Pixels_PixelsFloat_Impl__getPixelF(hxd__Pixels r0,int r1,int r2) {
	haxe__io__Bytes r9;
	h3d__Vector r7;
	double r8, r11, r13, r15;
	double *r10, *r12, *r14, *r16;
	int r3, r4, r5;
	if( r0 == NULL ) hl_null_access();
	r5 = r0->width;
	r4 = r2 * r5;
	r3 = r1 + r4;
	r4 = 4;
	r3 = r3 << r4;
	r4 = r0->offset;
	r3 = r3 + r4;
	r7 = (h3d__Vector)hl_alloc_obj(&t$h3d_Vector);
	r9 = r0->bytes;
	if( r9 == NULL ) hl_null_access();
	r8 = haxe_io_Bytes_getFloat(r9,r3);
	r10 = &r8;
	r9 = r0->bytes;
	if( r9 == NULL ) hl_null_access();
	r5 = 4;
	r4 = r3 + r5;
	r11 = haxe_io_Bytes_getFloat(r9,r4);
	r12 = &r11;
	r9 = r0->bytes;
	if( r9 == NULL ) hl_null_access();
	r5 = 8;
	r4 = r3 + r5;
	r13 = haxe_io_Bytes_getFloat(r9,r4);
	r14 = &r13;
	r9 = r0->bytes;
	if( r9 == NULL ) hl_null_access();
	r5 = 12;
	r4 = r3 + r5;
	r15 = haxe_io_Bytes_getFloat(r9,r4);
	r16 = &r15;
	h3d_Vector_new(r7,r10,r12,r14,r16);
	return r7;
}

void hxd__Pixels_PixelsFloat_Impl__setPixelF(hxd__Pixels r0,int r1,int r2,h3d__Vector r3) {
	haxe__io__Bytes r8;
	double r9;
	int r4, r5, r6;
	if( r0 == NULL ) hl_null_access();
	r6 = r0->width;
	r5 = r2 * r6;
	r4 = r1 + r5;
	r5 = 4;
	r4 = r4 << r5;
	r5 = r0->offset;
	r4 = r4 + r5;
	r8 = r0->bytes;
	if( r8 == NULL ) hl_null_access();
	if( r3 == NULL ) hl_null_access();
	r9 = r3->x;
	haxe_io_Bytes_setFloat(r8,r4,r9);
	r8 = r0->bytes;
	if( r8 == NULL ) hl_null_access();
	r6 = 4;
	r5 = r4 + r6;
	r9 = r3->y;
	haxe_io_Bytes_setFloat(r8,r5,r9);
	r8 = r0->bytes;
	if( r8 == NULL ) hl_null_access();
	r6 = 8;
	r5 = r4 + r6;
	r9 = r3->z;
	haxe_io_Bytes_setFloat(r8,r5,r9);
	r8 = r0->bytes;
	if( r8 == NULL ) hl_null_access();
	r6 = 12;
	r5 = r4 + r6;
	r9 = r3->w;
	haxe_io_Bytes_setFloat(r8,r5,r9);
	return;
}

hxd__Pixels hxd__Pixels_PixelsFloat_Impl__fromPixels(hxd__Pixels r0) {
	venum *r2;
	bool r3;
	if( r0 == NULL ) hl_null_access();
	r2 = (venum*)g$hxd_PixelFormat_RGBA32F;
	hxd_Pixels_convert(r0,r2);
	r3 = false;
	hxd_Pixels_setFlip(r0,r3);
	return r0;
}

