﻿// Generated by HLC 4.0.5 (HL v4)
#ifndef INC_h3d__anim__SmoothedObject
#define INC_h3d__anim__SmoothedObject
typedef struct _h3d__anim__$SmoothedObject *h3d__anim__$SmoothedObject;
typedef struct _h3d__anim__SmoothedObject *h3d__anim__SmoothedObject;
#include <hl/Class.h>
#include <hl/BaseType.h>
#include <_std/String.h>
#include <h3d/anim/AnimatedObject.h>
#include <h3d/scene/Object.h>
#include <h3d/scene/Skin.h>
#include <h3d/Matrix.h>


struct _h3d__anim__$SmoothedObject {
	hl_type *$type;
	hl_type* __type__;
	vdynamic* __meta__;
	varray* __implementedBy__;
	String __name__;
	vdynamic* __constructor__;
};
struct _h3d__anim__SmoothedObject {
	hl_type *$type;
	String objectName;
	h3d__scene__Object targetObject;
	h3d__scene__Skin targetSkin;
	int targetJoint;
	h3d__Matrix tmpMatrix;
	h3d__Matrix outMatrix;
	bool isAnim1;
	bool isAnim2;
	h3d__Matrix def;
};
#endif

