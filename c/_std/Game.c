﻿// Generated by HLC 4.0.5 (HL v4)
#define HLC_BOOT
#include <hlc.h>
#include <_std/Game.h>
extern venum* g$GameStates_START_SCREEN;
void Game_initBackground(Game);
void Game_initText(Game);
void Game_loadHighScore(Game);
void Game_checkForStart(Game);
void Game_startGame(Game);
void Game_updateShipPosition(Game);
void Game_updateAssets(Game);
void Game_checkForCollisions(Game);
void Game_updateHealthAndScore(Game);
void src_Background_update(src__Background);
#include <h2d/Object.h>
#include <h2d/Align.h>
h2d__Font hxd_res_DefaultFont_get(void);
extern hl_type t$h2d_Text;
void h2d_Text_new(h2d__Text,h2d__Font,h2d__Object);
extern String s$Tap_to_begin_;
String h2d_Text_set_text(h2d__Text,String);
extern venum* g$h2d_Align_Center;
venum* h2d_Text_set_textAlign(h2d__Text,venum*);
#include <h2d/Layers.h>
void h2d_Layers_addChild(h2d__Layers,h2d__Object);
extern String s$HIGH_SCORE_;
#include <hl/natives.h>
String String___alloc__(vbyte*,int);
String String___add__(String,String);
extern String s$HP_100;
extern venum* g$h2d_Align_Left;
extern String s$SCORE_0;
extern venum* g$h2d_Align_Right;
extern String s$GAME_OVER;
int h2d_Text_set_textColor(h2d__Text,int);
bool hxd_Key_isPressed(int);
bool hxd_Key_isDown(int);
extern venum* g$GameStates_TO_GAME;
extern hl_type t$src_Background;
void src_Background_new(src__Background,h2d__Scene);
extern String s$HP_;
int src_Ship_getHealth(src__Ship);
extern String s$SCORE_;
extern hl_type t$src_Ship;
void src_Ship_new(src__Ship,h2d__Scene);
void src_Ship_setScale(src__Ship,int);
void src_Ship_setPosition(src__Ship,int,int);
extern venum* g$GameStates_IN_GAME;
#include <src/Enemy.h>
void src_Enemy_die(src__Enemy);
extern hl_type t$src_Enemy;
hl__types__ArrayObj hl_types_ArrayObj_alloc(varray*);
extern venum* g$GameStates_GAME_OVER;
void src_Ship_update(src__Ship);
void src_Enemy_update(src__Enemy);
void src_Ship_move(src__Ship,int);
#include <h2d/Bitmap.h>
hl__types__ArrayObj src_Enemy_getProjectiles(src__Enemy);
h2d__Bitmap src_Ship_isHit(src__Ship,hl__types__ArrayObj);
int src_Enemy_getDamage(src__Enemy);
void src_Ship_takeDamage(src__Ship,int);
void src_Enemy_removeProjectile(src__Enemy,h2d__Bitmap);
bool src_Ship_isDed(src__Ship);
void src_Ship_die(src__Ship);
hl__types__ArrayObj src_Ship_getProjectiles(src__Ship);
h2d__Bitmap src_Enemy_isHit(src__Enemy,hl__types__ArrayObj);
int src_Ship_getDamage(src__Ship);
void src_Enemy_takeDamage(src__Enemy,int);
void src_Ship_removeProjectile(src__Ship,h2d__Bitmap);
bool src_Enemy_isDed(src__Enemy);
bool hl_types_ArrayObj_remove(hl__types__ArrayObj,vdynamic*);
void src_Enemy_new(src__Enemy,h2d__Scene,int);
int hl_types_ArrayObj_push(hl__types__ArrayObj,vdynamic*);
#include <hxd/res/Loader.h>
#include <hxd/fs/LocalFileSystem.h>
#include <hxd/fs/FileEntry.h>
extern hl_type t$hxd_res_Loader;
extern hl_type t$hxd_fs_LocalFileSystem;
extern String s$res;
void hxd_fs_LocalFileSystem_new(hxd__fs__LocalFileSystem,String,String);
extern hl_type t$vrt_81c24d5;
void hxd_res_Loader_new(hxd__res__Loader,vvirtual*);
hxd__res__Loader hxd_Res_set_loader(hxd__res__Loader);
extern hl_type t$Game;
void Game_new(Game);
void hxd_App_new(hxd__App);

void Game_init(Game r0) {
	venum *r1;
	r1 = (venum*)g$GameStates_START_SCREEN;
	r0->game_state = r1;
	Game_initBackground(r0);
	Game_initText(r0);
	Game_loadHighScore(r0);
	return;
}

void Game_update(Game r0,double r1) {
	venum *r4;
	src__Background r5;
	int r3;
	r4 = r0->game_state;
	if( r4 == NULL ) hl_null_access();
	r3 = HL__ENUM_INDEX__(r4);
	switch(r3) {
		default:
			goto label$ccc71f7_2_16;
		case 0:
			Game_checkForStart(r0);
			goto label$ccc71f7_2_16;
		case 1:
			Game_startGame(r0);
			goto label$ccc71f7_2_16;
		case 2:
			Game_updateShipPosition(r0);
			Game_updateAssets(r0);
			Game_checkForCollisions(r0);
			Game_updateHealthAndScore(r0);
			goto label$ccc71f7_2_16;
		case 3:
			goto label$ccc71f7_2_16;
		case 4:
			Game_checkForStart(r0);
	}
	label$ccc71f7_2_16:
	r5 = r0->background;
	if( r5 == NULL ) hl_null_access();
	src_Background_update(r5);
	return;
}

void Game_initText(Game r0) {
	String r5, r14;
	venum *r6;
	bool r11;
	h2d__Object r3;
	h2d__Text r2;
	h2d__Font r1;
	double r7, r10;
	int *r12;
	h2d__Scene r9;
	vbyte *r13;
	int r8, r15;
	r1 = hxd_res_DefaultFont_get();
	r0->font = r1;
	r2 = (h2d__Text)hl_alloc_obj(&t$h2d_Text);
	r1 = r0->font;
	r3 = NULL;
	h2d_Text_new(r2,r1,r3);
	r0->start_text = r2;
	r2 = r0->start_text;
	if( r2 == NULL ) hl_null_access();
	r5 = (String)s$Tap_to_begin_;
	r5 = h2d_Text_set_text(r2,r5);
	r2 = r0->start_text;
	if( r2 == NULL ) hl_null_access();
	r6 = (venum*)g$h2d_Align_Center;
	r6 = h2d_Text_set_textAlign(r2,r6);
	r2 = r0->start_text;
	r9 = r0->s2d;
	if( r9 == NULL ) hl_null_access();
	r8 = r9->width;
	r7 = (double)r8;
	r10 = 2.;
	r7 = r7 / r10;
	r8 = (int)r7;
	r10 = (double)r8;
	if( r2 == NULL ) hl_null_access();
	r11 = true;
	r2->posChanged = r11;
	r2->x = r10;
	r2 = r0->start_text;
	r9 = r0->s2d;
	if( r9 == NULL ) hl_null_access();
	r8 = r9->height;
	r7 = (double)r8;
	r10 = 2.;
	r7 = r7 / r10;
	r8 = (int)r7;
	r10 = (double)r8;
	if( r2 == NULL ) hl_null_access();
	r11 = true;
	r2->posChanged = r11;
	r2->y = r10;
	r2 = r0->start_text;
	if( r2 == NULL ) hl_null_access();
	r11 = true;
	r2->posChanged = r11;
	r7 = r2->scaleX;
	r10 = 3.;
	r7 = r7 * r10;
	r2->scaleX = r7;
	r11 = true;
	r2->posChanged = r11;
	r7 = r2->scaleY;
	r10 = 3.;
	r7 = r7 * r10;
	r2->scaleY = r7;
	r2 = r0->start_text;
	if( r2 == NULL ) hl_null_access();
	r7 = 1.;
	r2->alpha = r7;
	r9 = r0->s2d;
	if( r9 == NULL ) hl_null_access();
	r2 = r0->start_text;
	h2d_Layers_addChild(((h2d__Layers)r9),((h2d__Object)r2));
	r2 = (h2d__Text)hl_alloc_obj(&t$h2d_Text);
	r1 = r0->font;
	r3 = NULL;
	h2d_Text_new(r2,r1,r3);
	r0->high_score_text = r2;
	r2 = r0->high_score_text;
	if( r2 == NULL ) hl_null_access();
	r5 = (String)s$HIGH_SCORE_;
	r8 = r0->high_score;
	r12 = &r8;
	r13 = hl_itos(r8,r12);
	r14 = String___alloc__(r13,r8);
	r5 = String___add__(r5,r14);
	r5 = h2d_Text_set_text(r2,r5);
	r2 = r0->high_score_text;
	if( r2 == NULL ) hl_null_access();
	r6 = (venum*)g$h2d_Align_Center;
	r6 = h2d_Text_set_textAlign(r2,r6);
	r2 = r0->high_score_text;
	r9 = r0->s2d;
	if( r9 == NULL ) hl_null_access();
	r8 = r9->width;
	r7 = (double)r8;
	r10 = 2.;
	r7 = r7 / r10;
	r8 = (int)r7;
	r10 = (double)r8;
	if( r2 == NULL ) hl_null_access();
	r11 = true;
	r2->posChanged = r11;
	r2->x = r10;
	r2 = r0->high_score_text;
	r8 = 3;
	r9 = r0->s2d;
	if( r9 == NULL ) hl_null_access();
	r15 = r9->height;
	r8 = r8 * r15;
	r7 = (double)r8;
	r10 = 4.;
	r7 = r7 / r10;
	r8 = (int)r7;
	r10 = (double)r8;
	if( r2 == NULL ) hl_null_access();
	r11 = true;
	r2->posChanged = r11;
	r2->y = r10;
	r2 = r0->high_score_text;
	if( r2 == NULL ) hl_null_access();
	r11 = true;
	r2->posChanged = r11;
	r7 = r2->scaleX;
	r10 = 2.;
	r7 = r7 * r10;
	r2->scaleX = r7;
	r11 = true;
	r2->posChanged = r11;
	r7 = r2->scaleY;
	r10 = 2.;
	r7 = r7 * r10;
	r2->scaleY = r7;
	r9 = r0->s2d;
	if( r9 == NULL ) hl_null_access();
	r2 = r0->high_score_text;
	h2d_Layers_addChild(((h2d__Layers)r9),((h2d__Object)r2));
	r2 = (h2d__Text)hl_alloc_obj(&t$h2d_Text);
	r1 = r0->font;
	r3 = NULL;
	h2d_Text_new(r2,r1,r3);
	r0->hp_text = r2;
	r2 = r0->hp_text;
	if( r2 == NULL ) hl_null_access();
	r5 = (String)s$HP_100;
	r5 = h2d_Text_set_text(r2,r5);
	r2 = r0->hp_text;
	if( r2 == NULL ) hl_null_access();
	r6 = (venum*)g$h2d_Align_Left;
	r6 = h2d_Text_set_textAlign(r2,r6);
	r2 = r0->hp_text;
	if( r2 == NULL ) hl_null_access();
	r11 = true;
	r2->posChanged = r11;
	r7 = 20.;
	r2->x = r7;
	r2 = r0->hp_text;
	if( r2 == NULL ) hl_null_access();
	r11 = true;
	r2->posChanged = r11;
	r7 = 17.;
	r2->y = r7;
	r2 = r0->hp_text;
	if( r2 == NULL ) hl_null_access();
	r7 = 0.;
	r2->alpha = r7;
	r2 = r0->hp_text;
	if( r2 == NULL ) hl_null_access();
	r11 = true;
	r2->posChanged = r11;
	r7 = 2.;
	r2->scaleX = r7;
	r11 = true;
	r2->posChanged = r11;
	r7 = 2.;
	r2->scaleY = r7;
	r9 = r0->s2d;
	if( r9 == NULL ) hl_null_access();
	r2 = r0->hp_text;
	h2d_Layers_addChild(((h2d__Layers)r9),((h2d__Object)r2));
	r2 = (h2d__Text)hl_alloc_obj(&t$h2d_Text);
	r1 = r0->font;
	r3 = NULL;
	h2d_Text_new(r2,r1,r3);
	r0->score_text = r2;
	r2 = r0->score_text;
	if( r2 == NULL ) hl_null_access();
	r5 = (String)s$SCORE_0;
	r5 = h2d_Text_set_text(r2,r5);
	r2 = r0->score_text;
	if( r2 == NULL ) hl_null_access();
	r6 = (venum*)g$h2d_Align_Right;
	r6 = h2d_Text_set_textAlign(r2,r6);
	r2 = r0->score_text;
	if( r2 == NULL ) hl_null_access();
	r11 = true;
	r2->posChanged = r11;
	r9 = r0->s2d;
	if( r9 == NULL ) hl_null_access();
	r8 = r9->width;
	r15 = 20;
	r8 = r8 - r15;
	r7 = (double)r8;
	r2->x = r7;
	r2 = r0->score_text;
	if( r2 == NULL ) hl_null_access();
	r11 = true;
	r2->posChanged = r11;
	r7 = 17.;
	r2->y = r7;
	r2 = r0->score_text;
	if( r2 == NULL ) hl_null_access();
	r7 = 0.;
	r2->alpha = r7;
	r2 = r0->score_text;
	if( r2 == NULL ) hl_null_access();
	r11 = true;
	r2->posChanged = r11;
	r7 = 2.;
	r2->scaleX = r7;
	r11 = true;
	r2->posChanged = r11;
	r7 = 2.;
	r2->scaleY = r7;
	r9 = r0->s2d;
	if( r9 == NULL ) hl_null_access();
	r2 = r0->score_text;
	h2d_Layers_addChild(((h2d__Layers)r9),((h2d__Object)r2));
	r2 = (h2d__Text)hl_alloc_obj(&t$h2d_Text);
	r1 = r0->font;
	r3 = NULL;
	h2d_Text_new(r2,r1,r3);
	r0->game_over_text = r2;
	r2 = r0->game_over_text;
	if( r2 == NULL ) hl_null_access();
	r5 = (String)s$GAME_OVER;
	r5 = h2d_Text_set_text(r2,r5);
	r2 = r0->game_over_text;
	if( r2 == NULL ) hl_null_access();
	r6 = (venum*)g$h2d_Align_Center;
	r6 = h2d_Text_set_textAlign(r2,r6);
	r2 = r0->game_over_text;
	if( r2 == NULL ) hl_null_access();
	r8 = 16711680;
	r8 = h2d_Text_set_textColor(r2,r8);
	r2 = r0->game_over_text;
	r9 = r0->s2d;
	if( r9 == NULL ) hl_null_access();
	r8 = r9->width;
	r7 = (double)r8;
	r10 = 2.;
	r7 = r7 / r10;
	r8 = (int)r7;
	r10 = (double)r8;
	if( r2 == NULL ) hl_null_access();
	r11 = true;
	r2->posChanged = r11;
	r2->x = r10;
	r2 = r0->game_over_text;
	r9 = r0->s2d;
	if( r9 == NULL ) hl_null_access();
	r8 = r9->height;
	r7 = (double)r8;
	r10 = 4.;
	r7 = r7 / r10;
	r8 = (int)r7;
	r10 = (double)r8;
	if( r2 == NULL ) hl_null_access();
	r11 = true;
	r2->posChanged = r11;
	r2->y = r10;
	r2 = r0->game_over_text;
	if( r2 == NULL ) hl_null_access();
	r11 = true;
	r2->posChanged = r11;
	r7 = 4.;
	r2->scaleX = r7;
	r11 = true;
	r2->posChanged = r11;
	r7 = 4.;
	r2->scaleY = r7;
	r2 = r0->game_over_text;
	if( r2 == NULL ) hl_null_access();
	r7 = 0.;
	r2->alpha = r7;
	r9 = r0->s2d;
	if( r9 == NULL ) hl_null_access();
	r2 = r0->game_over_text;
	h2d_Layers_addChild(((h2d__Layers)r9),((h2d__Object)r2));
	return;
}

void Game_loadHighScore(Game r0) {
	return;
}

void Game_checkForStart(Game r0) {
	venum *r4;
	bool r2;
	int r3;
	r3 = 88;
	r2 = hxd_Key_isPressed(r3);
	if( r2 ) goto label$ccc71f7_5_6;
	r3 = 88;
	r2 = hxd_Key_isDown(r3);
	if( !r2 ) goto label$ccc71f7_5_8;
	label$ccc71f7_5_6:
	r4 = (venum*)g$GameStates_TO_GAME;
	r0->game_state = r4;
	label$ccc71f7_5_8:
	return;
}

void Game_updateHighScore(Game r0) {
	int r2, r3;
	r2 = r0->score;
	r3 = r0->high_score;
	if( r3 >= r2 ) goto label$ccc71f7_6_5;
	r2 = r0->score;
	r0->high_score = r2;
	label$ccc71f7_6_5:
	return;
}

void Game_initBackground(Game r0) {
	src__Background r1;
	h2d__Scene r2;
	r1 = (src__Background)hl_alloc_obj(&t$src_Background);
	r2 = r0->s2d;
	src_Background_new(r1,r2);
	r0->background = r1;
	return;
}

void Game_updateHealthAndScore(Game r0) {
	String r1, r7;
	src__Ship r4;
	h2d__Text r2;
	int *r5;
	vbyte *r6;
	int r3;
	r2 = r0->hp_text;
	if( r2 == NULL ) hl_null_access();
	r1 = (String)s$HP_;
	r4 = r0->ship;
	if( r4 == NULL ) hl_null_access();
	r3 = src_Ship_getHealth(r4);
	r5 = &r3;
	r6 = hl_itos(r3,r5);
	r7 = String___alloc__(r6,r3);
	r1 = String___add__(r1,r7);
	r1 = h2d_Text_set_text(r2,r1);
	r2 = r0->score_text;
	if( r2 == NULL ) hl_null_access();
	r1 = (String)s$SCORE_;
	r3 = r0->score;
	r5 = &r3;
	r6 = hl_itos(r3,r5);
	r7 = String___alloc__(r6,r3);
	r1 = String___add__(r1,r7);
	r1 = h2d_Text_set_text(r2,r1);
	return;
}

void Game_startGame(Game r0) {
	venum *r11;
	src__Ship r1;
	h2d__Text r10;
	double r5, r6, r9;
	h2d__Scene r2;
	int r4, r7, r8;
	r1 = (src__Ship)hl_alloc_obj(&t$src_Ship);
	r2 = r0->s2d;
	src_Ship_new(r1,r2);
	r0->ship = r1;
	r1 = r0->ship;
	if( r1 == NULL ) hl_null_access();
	r4 = 2;
	src_Ship_setScale(r1,r4);
	r1 = r0->ship;
	r2 = r0->s2d;
	if( r2 == NULL ) hl_null_access();
	r4 = r2->width;
	r5 = (double)r4;
	r6 = 2.;
	r5 = r5 / r6;
	r6 = 14.;
	r5 = r5 - r6;
	r4 = (int)r5;
	r7 = 3;
	r2 = r0->s2d;
	if( r2 == NULL ) hl_null_access();
	r8 = r2->height;
	r7 = r7 * r8;
	r6 = (double)r7;
	r9 = 4.;
	r6 = r6 / r9;
	if( r1 == NULL ) hl_null_access();
	r8 = (int)r6;
	src_Ship_setPosition(r1,r4,r8);
	r10 = r0->game_over_text;
	if( r10 == NULL ) hl_null_access();
	r9 = 0.;
	r10->alpha = r9;
	r10 = r0->start_text;
	if( r10 == NULL ) hl_null_access();
	r9 = 0.;
	r10->alpha = r9;
	r10 = r0->high_score_text;
	if( r10 == NULL ) hl_null_access();
	r9 = 0.;
	r10->alpha = r9;
	r10 = r0->hp_text;
	if( r10 == NULL ) hl_null_access();
	r9 = 1.;
	r10->alpha = r9;
	r10 = r0->score_text;
	if( r10 == NULL ) hl_null_access();
	r9 = 1.;
	r10->alpha = r9;
	r11 = (venum*)g$GameStates_IN_GAME;
	r0->game_state = r11;
	return;
}

void Game_gameOver(Game r0) {
	String r9, r13;
	src__Enemy r5;
	hl__types__ArrayObj r3;
	venum *r15;
	hl_type *r8;
	h2d__Text r10;
	double r14;
	int *r11;
	vdynamic *r6;
	vbyte *r12;
	varray *r7;
	int r2, r4;
	Game_updateHighScore(r0);
	r2 = 0;
	r3 = r0->enemies;
	label$ccc71f7_10_3:
	if( r3 == NULL ) hl_null_access();
	r4 = r3->length;
	if( r2 >= r4 ) goto label$ccc71f7_10_18;
	r4 = r3->length;
	if( ((unsigned)r2) < ((unsigned)r4) ) goto label$ccc71f7_10_11;
	r5 = NULL;
	goto label$ccc71f7_10_14;
	label$ccc71f7_10_11:
	r7 = r3->array;
	r6 = ((vdynamic**)(r7 + 1))[r2];
	r5 = (src__Enemy)r6;
	label$ccc71f7_10_14:
	++r2;
	if( r5 == NULL ) hl_null_access();
	src_Enemy_die(r5);
	goto label$ccc71f7_10_3;
	label$ccc71f7_10_18:
	r8 = &t$src_Enemy;
	r2 = 0;
	r7 = hl_alloc_array(r8,r2);
	r3 = hl_types_ArrayObj_alloc(r7);
	r0->enemies = r3;
	r2 = 0;
	r0->score = r2;
	r10 = r0->high_score_text;
	if( r10 == NULL ) hl_null_access();
	r9 = (String)s$HIGH_SCORE_;
	r2 = r0->high_score;
	r11 = &r2;
	r12 = hl_itos(r2,r11);
	r13 = String___alloc__(r12,r2);
	r9 = String___add__(r9,r13);
	r9 = h2d_Text_set_text(r10,r9);
	r10 = r0->hp_text;
	if( r10 == NULL ) hl_null_access();
	r14 = 0.;
	r10->alpha = r14;
	r10 = r0->score_text;
	if( r10 == NULL ) hl_null_access();
	r14 = 0.;
	r10->alpha = r14;
	r10 = r0->game_over_text;
	if( r10 == NULL ) hl_null_access();
	r14 = 1.;
	r10->alpha = r14;
	r10 = r0->start_text;
	if( r10 == NULL ) hl_null_access();
	r14 = 1.;
	r10->alpha = r14;
	r10 = r0->high_score_text;
	if( r10 == NULL ) hl_null_access();
	r14 = 1.;
	r10->alpha = r14;
	r15 = (venum*)g$GameStates_GAME_OVER;
	r0->game_state = r15;
	return;
}

void Game_updateAssets(Game r0) {
	src__Enemy r6;
	hl__types__ArrayObj r4;
	src__Ship r2;
	vdynamic *r7;
	varray *r8;
	int r3, r5;
	r2 = r0->ship;
	if( r2 == NULL ) hl_null_access();
	src_Ship_update(r2);
	r3 = 0;
	r4 = r0->enemies;
	label$ccc71f7_11_5:
	if( r4 == NULL ) hl_null_access();
	r5 = r4->length;
	if( r3 >= r5 ) goto label$ccc71f7_11_20;
	r5 = r4->length;
	if( ((unsigned)r3) < ((unsigned)r5) ) goto label$ccc71f7_11_13;
	r6 = NULL;
	goto label$ccc71f7_11_16;
	label$ccc71f7_11_13:
	r8 = r4->array;
	r7 = ((vdynamic**)(r8 + 1))[r3];
	r6 = (src__Enemy)r7;
	label$ccc71f7_11_16:
	++r3;
	if( r6 == NULL ) hl_null_access();
	src_Enemy_update(r6);
	goto label$ccc71f7_11_5;
	label$ccc71f7_11_20:
	return;
}

void Game_updateShipPosition(Game r0) {
	bool r2;
	src__Ship r4;
	int r3;
	r3 = 87;
	r2 = hxd_Key_isPressed(r3);
	if( r2 ) goto label$ccc71f7_12_6;
	r3 = 87;
	r2 = hxd_Key_isDown(r3);
	if( !r2 ) goto label$ccc71f7_12_10;
	label$ccc71f7_12_6:
	r4 = r0->ship;
	if( r4 == NULL ) hl_null_access();
	r3 = 0;
	src_Ship_move(r4,r3);
	label$ccc71f7_12_10:
	r3 = 68;
	r2 = hxd_Key_isPressed(r3);
	if( r2 ) goto label$ccc71f7_12_16;
	r3 = 68;
	r2 = hxd_Key_isDown(r3);
	if( !r2 ) goto label$ccc71f7_12_20;
	label$ccc71f7_12_16:
	r4 = r0->ship;
	if( r4 == NULL ) hl_null_access();
	r3 = 1;
	src_Ship_move(r4,r3);
	label$ccc71f7_12_20:
	r3 = 83;
	r2 = hxd_Key_isPressed(r3);
	if( r2 ) goto label$ccc71f7_12_26;
	r3 = 83;
	r2 = hxd_Key_isDown(r3);
	if( !r2 ) goto label$ccc71f7_12_30;
	label$ccc71f7_12_26:
	r4 = r0->ship;
	if( r4 == NULL ) hl_null_access();
	r3 = 2;
	src_Ship_move(r4,r3);
	label$ccc71f7_12_30:
	r3 = 65;
	r2 = hxd_Key_isPressed(r3);
	if( r2 ) goto label$ccc71f7_12_36;
	r3 = 65;
	r2 = hxd_Key_isDown(r3);
	if( !r2 ) goto label$ccc71f7_12_40;
	label$ccc71f7_12_36:
	r4 = r0->ship;
	if( r4 == NULL ) hl_null_access();
	r3 = 3;
	src_Ship_move(r4,r3);
	label$ccc71f7_12_40:
	return;
}

void Game_checkForCollisions(Game r0) {
	src__Enemy r8;
	hl__types__ArrayObj r4, r7;
	bool r14;
	h2d__Object r13;
	src__Ship r12;
	h2d__Bitmap r1, r11;
	h2d__Scene r15;
	vdynamic *r9;
	varray *r10;
	int r3, r5, r6;
	r1 = NULL;
	r3 = 0;
	r4 = r0->enemies;
	label$ccc71f7_13_3:
	if( r4 == NULL ) hl_null_access();
	r6 = r4->length;
	if( r3 >= r6 ) goto label$ccc71f7_13_73;
	r6 = r4->length;
	if( ((unsigned)r3) < ((unsigned)r6) ) goto label$ccc71f7_13_11;
	r8 = NULL;
	goto label$ccc71f7_13_14;
	label$ccc71f7_13_11:
	r10 = r4->array;
	r9 = ((vdynamic**)(r10 + 1))[r3];
	r8 = (src__Enemy)r9;
	label$ccc71f7_13_14:
	++r3;
	r12 = r0->ship;
	if( r12 == NULL ) hl_null_access();
	if( r8 == NULL ) hl_null_access();
	r7 = src_Enemy_getProjectiles(r8);
	r11 = src_Ship_isHit(r12,r7);
	if( !r11 ) goto label$ccc71f7_13_42;
	r12 = r0->ship;
	if( r12 == NULL ) hl_null_access();
	r5 = src_Enemy_getDamage(r8);
	src_Ship_takeDamage(r12,r5);
	src_Enemy_removeProjectile(r8,r11);
	if( !r11 ) goto label$ccc71f7_13_33;
	if( r11 == NULL ) hl_null_access();
	r13 = r11->parent;
	if( !r13 ) goto label$ccc71f7_13_33;
	r13 = r11->parent;
	if( r13 == NULL ) hl_null_access();
	((void (*)(h2d__Object,h2d__Bitmap))r13->$type->vobj_proto[6])(r13,r11);
	label$ccc71f7_13_33:
	r11 = NULL;
	r12 = r0->ship;
	if( r12 == NULL ) hl_null_access();
	r14 = src_Ship_isDed(r12);
	if( !r14 ) goto label$ccc71f7_13_42;
	r12 = r0->ship;
	if( r12 == NULL ) hl_null_access();
	src_Ship_die(r12);
	Game_gameOver(r0);
	label$ccc71f7_13_42:
	r12 = r0->ship;
	if( r12 == NULL ) hl_null_access();
	r7 = src_Ship_getProjectiles(r12);
	r11 = src_Enemy_isHit(r8,r7);
	if( !r11 ) goto label$ccc71f7_13_72;
	r12 = r0->ship;
	if( r12 == NULL ) hl_null_access();
	r5 = src_Ship_getDamage(r12);
	src_Enemy_takeDamage(r8,r5);
	r12 = r0->ship;
	if( r12 == NULL ) hl_null_access();
	src_Ship_removeProjectile(r12,r11);
	if( !r11 ) goto label$ccc71f7_13_61;
	if( r11 == NULL ) hl_null_access();
	r13 = r11->parent;
	if( !r13 ) goto label$ccc71f7_13_61;
	r13 = r11->parent;
	if( r13 == NULL ) hl_null_access();
	((void (*)(h2d__Object,h2d__Bitmap))r13->$type->vobj_proto[6])(r13,r11);
	label$ccc71f7_13_61:
	r11 = NULL;
	r14 = src_Enemy_isDed(r8);
	if( !r14 ) goto label$ccc71f7_13_72;
	r5 = r0->score;
	r6 = r8->score;
	r5 = r5 + r6;
	r0->score = r5;
	src_Enemy_die(r8);
	r7 = r0->enemies;
	if( r7 == NULL ) hl_null_access();
	r14 = hl_types_ArrayObj_remove(r7,((vdynamic*)r8));
	label$ccc71f7_13_72:
	goto label$ccc71f7_13_3;
	label$ccc71f7_13_73:
	r3 = r0->frames_after_enemy;
	r5 = 80;
	if( r3 != r5 ) goto label$ccc71f7_13_86;
	r4 = r0->enemies;
	if( r4 == NULL ) hl_null_access();
	r8 = (src__Enemy)hl_alloc_obj(&t$src_Enemy);
	r15 = r0->s2d;
	r3 = 2;
	src_Enemy_new(r8,r15,r3);
	r3 = hl_types_ArrayObj_push(r4,((vdynamic*)r8));
	r3 = 0;
	r0->frames_after_enemy = r3;
	goto label$ccc71f7_13_94;
	label$ccc71f7_13_86:
	r4 = r0->enemies;
	if( r4 == NULL ) hl_null_access();
	r3 = r4->length;
	r5 = 8;
	if( r3 >= r5 ) goto label$ccc71f7_13_94;
	r3 = r0->frames_after_enemy;
	++r3;
	r0->frames_after_enemy = r3;
	label$ccc71f7_13_94:
	return;
}

void Game_displayHpAndScore(Game r0) {
	return;
}

void Game_main() {
	String r2, r3;
	Game r6;
	vvirtual *r5;
	hxd__fs__LocalFileSystem r1;
	hxd__res__Loader r0;
	r0 = (hxd__res__Loader)hl_alloc_obj(&t$hxd_res_Loader);
	r1 = (hxd__fs__LocalFileSystem)hl_alloc_obj(&t$hxd_fs_LocalFileSystem);
	r2 = (String)s$res;
	r3 = NULL;
	hxd_fs_LocalFileSystem_new(r1,r2,r3);
	if( r1 ) goto label$ccc71f7_15_8;
	r5 = NULL;
	goto label$ccc71f7_15_12;
	label$ccc71f7_15_8:
	r5 = r1->f$5;
	if( r5 ) goto label$ccc71f7_15_12;
	r5 = hl_to_virtual(&t$vrt_81c24d5,(vdynamic*)r1);
	r1->f$5 = r5;
	label$ccc71f7_15_12:
	hxd_res_Loader_new(r0,r5);
	r0 = hxd_Res_set_loader(r0);
	r6 = (Game)hl_alloc_obj(&t$Game);
	Game_new(r6);
	return;
}

void Game_new(Game r0) {
	hl__types__ArrayObj r2;
	hl_type *r4;
	varray *r3;
	int r1;
	r1 = 0;
	r0->high_score = r1;
	r1 = 0;
	r0->score = r1;
	r1 = 0;
	r0->frames_after_enemy = r1;
	r4 = &t$src_Enemy;
	r1 = 0;
	r3 = hl_alloc_array(r4,r1);
	r2 = hl_types_ArrayObj_alloc(r3);
	r0->enemies = r2;
	hxd_App_new(((hxd__App)r0));
	return;
}

